package BaseClasses;

public class Models {
    private String loginuserType;

    public String getLoginuserType() {
        return loginuserType;
    }

    public void setLoginuserType(String loginuserType) {
        this.loginuserType = loginuserType;
    }

    @Override
    public String toString() {
        return "Models{" +
                "loginuserType='" + loginuserType + '\'' +
                '}';
    }
}
