package BaseClasses;

import com.paulhammant.ngwebdriver.NgWebDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.*;


import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;


public class TestBase {

    public static WebDriver driver;
    public  static Properties prop=new Properties();
    String env ="Testdata";
    public static String mobilenumberforreg="";
    public static  String fileName = "TEST-RESULT-"+String.valueOf(System.currentTimeMillis());
    public static String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());


    @Parameters({"browser.name"})
    @BeforeClass
    public void beforeMethod(@Optional("chrome") String browser)
    {
        setup_file();
        if (browser.equalsIgnoreCase("chrome")) {
            WebDriverManager.chromedriver().setup();
            ChromeOptions options = new ChromeOptions();
//            Remove the message of "Chrome is bieng controlled by Automated Test Software" in the browser
            options.addArguments("disable-infobars");
//            Maximize the browser
            options.addArguments("start-maximized");
            driver = new ChromeDriver(options);
        } else if (browser.equalsIgnoreCase("firefox")) {
            driver = new FirefoxDriver();
        } else if (browser.equalsIgnoreCase("ie")) {

        } else if (browser.equalsIgnoreCase("edge")) {

        } else if (browser.equalsIgnoreCase("safari")) {

        } else if (browser.equalsIgnoreCase("chrome-headless")) {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("headless");
            driver = new ChromeDriver(options);

        } else if (browser.equalsIgnoreCase("firefox-headless")) {
            FirefoxBinary firefoxBinary = new FirefoxBinary();
            firefoxBinary.addCommandLineOptions("--headless");
            FirefoxOptions options = new FirefoxOptions();
            options.setBinary(firefoxBinary);
            driver = new FirefoxDriver(options);

        } else {
            System.exit(-1);
        }
        JavascriptExecutor jsDriver = (JavascriptExecutor) driver;
        NgWebDriver ngWebDriver = new NgWebDriver(jsDriver);
        driver.manage().window().maximize();

          driver.navigate().to(Constants.BASE_URL);

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        ngWebDriver.waitForAngularRequestsToFinish();
    }

    private void setup_file()
    {

        {

            try {
                if(env.equals("Testdata"))
                    prop.load(new FileInputStream("C:\\Users\\sadisha\\IdeaProjects\\HelaviruFrameWork\\src\\test\\Utill\\qa.properties"));
                else if(env.equals("env2"))
                {
                    prop.load(new FileInputStream("C:\\Users\\sadisha\\IdeaProjects\\helaviru\\src\\test\\Utill\\data\\qa2.properties"));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }








    @AfterClass
    public void afterSuite() throws InterruptedException
    {

       /* driver.quit();
        Thread.sleep(2000);
*/
    }


}
