package BaseClasses;

import com.paulhammant.ngwebdriver.NgWebDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBaseAdmin {


    public static WebDriver driver;
    public  static Properties prop=new Properties();
    String env ="Testdata";



    @Parameters({"browser.name"})
    @BeforeMethod
    public void beforeMethod(@Optional("chrome") String browser)
    {
        setup_file();
        if (browser.equalsIgnoreCase("chrome")) {
            WebDriverManager.chromedriver().setup();
            ChromeOptions options = new ChromeOptions();
            System.out.println("Chrome driver selected successfully ");
//            Remove the message of "Chrome is bieng controlled by Automated Test Software" in the browser
            options.addArguments("disable-infobars");
//            Maximize the browser
            options.addArguments("start-maximized");
            driver = new ChromeDriver(options);
        } else if (browser.equalsIgnoreCase("firefox")) {
            driver = new FirefoxDriver();
            System.out.println("firefox driver selected successfully ");
        } else if (browser.equalsIgnoreCase("ie"))
        {
            System.out.println("firefox driver selected successfully ");
        } else if (browser.equalsIgnoreCase("edge")) {
            System.out.println("firefox driver selected successfully ");

        } else if (browser.equalsIgnoreCase("safari")) {

        } else if (browser.equalsIgnoreCase("chrome-headless")) {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("headless");
            driver = new ChromeDriver(options);
            System.out.println("firefox driver selected successfully ");

        } else if (browser.equalsIgnoreCase("firefox-headless")) {
            FirefoxBinary firefoxBinary = new FirefoxBinary();
            firefoxBinary.addCommandLineOptions("--headless");
            FirefoxOptions options = new FirefoxOptions();
            options.setBinary(firefoxBinary);
            driver = new FirefoxDriver(options);
            System.out.println("firefox driver selected successfully ");

        } else {
            System.exit(-1);
        }
        JavascriptExecutor jsDriver = (JavascriptExecutor) driver;
        NgWebDriver ngWebDriver = new NgWebDriver(jsDriver);
        driver.manage().window().maximize();




       driver.navigate().to(Constants.AdminBASE_URL);

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        ngWebDriver.waitForAngularRequestsToFinish();
    }

    private void setup_file()
    {

        {

            try {
                if(env.equals("Testdata"))
                {
                    prop.load(new FileInputStream("C:\\Users\\sadisha\\IdeaProjects\\HelaviruFrameWork\\src\\test\\Utill\\qa.properties"));
                    System.out.println("file Selected Successfully ");
                }
                else if(env.equals("env2"))
                {
                    prop.load(new FileInputStream("C:\\Users\\sadisha\\IdeaProjects\\helaviru\\src\\test\\Utill\\data\\qa2.properties"));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }








    @AfterMethod
    public void afterSuite() throws InterruptedException
    {

      //  driver.quit();
        Thread.sleep(2000);
    }




}
