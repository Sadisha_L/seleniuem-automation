package AdminTestCases;

import AdminPages.AdminDashBoardPage;

import BaseClasses.TestBaseAdmin;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import static AdminTestCases.TC02_Admin_Verify_Added_Products.userlogin;

public class Admin_VerifyPaymentRecordISAvilable extends TestBaseAdmin {
    @Test
    public  static void verify_payment_Record_is_Available() throws InterruptedException {
        AdminDashBoardPage adminDashBoardPage = PageFactory.initElements(driver, AdminDashBoardPage.class);
        userlogin();
        Thread.sleep(1000);
        adminDashBoardPage.scrollDownInWindow();
        Thread.sleep(2000);
        adminDashBoardPage.clickOnpayment_Verification();
        Thread.sleep(2000);
        driver.navigate().refresh();
        Thread.sleep(5000);
        adminDashBoardPage.search_transaction("D6B36E4AB5");
        Thread.sleep(2000);
        String value="D6B36E4AB5";
        Assert.assertEquals(adminDashBoardPage.verify_transactionIsThere(),value);

    }

    @AfterTest
    public  void teardown()
    {
        driver.quit();
}

}
