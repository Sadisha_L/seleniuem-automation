package AdminTestCases;

import AdminPages.AdminDashBoardPage;
import AdminPages.AdminLoginPage;

import BaseClasses.Constants;
import BaseClasses.CreateCsvFile;
import BaseClasses.TestBaseAdmin;
import UserTestCases.ValidateSellerAbleTobuyProductsViaBank;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import static BaseClasses.TestBase.timeStamp;


public class TC01_Admin_LoginTo_System extends TestBaseAdmin {



    @Test
    public static void loginTo_system() throws InterruptedException
    {
        AdminLoginPage adminloginpg = PageFactory.initElements(driver, AdminLoginPage.class);
        CreateCsvFile.generateCsvFile("************************","exCreate");
        CreateCsvFile.generateCsvFile("Admin Login to System test case Started =="+ TC01_Admin_LoginTo_System.class,timeStamp);
        adminloginpg.enterUsername(Constants.ADMIN_USERNAME);
        Thread.sleep(2000);
        adminloginpg.enterPassword(Constants.ADMIN_PASSWORD);
        Thread.sleep(2000);
        AdminDashBoardPage dashBoardPage =adminloginpg.clickOnLoginBtn();
        Thread.sleep(7000);
        Assert.assertEquals(dashBoardPage.VerifyAdminUserLoginToSystem(),"Overview");
        CreateCsvFile.generateCsvFile("************************",timeStamp);
        CreateCsvFile.generateCsvFile("Admin Login to System test case Finished =="+ TC01_Admin_LoginTo_System.class,timeStamp);

    }

    @AfterTest
    public  void teardown()
    {
        driver.quit();
    }


}
