package AdminTestCases;

import AdminPages.AdminDashBoardPage;
import BaseClasses.CreateCsvFile;
import BaseClasses.TestBaseAdmin;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import static BaseClasses.TestBase.timeStamp;

public class TC03_Admin_Approve_Added_Product extends TestBaseAdmin {

    @Test
    public void approve_addedProducts() throws InterruptedException {
      AdminDashBoardPage adminDashBoardPage = PageFactory.initElements(driver, AdminDashBoardPage.class);
        CreateCsvFile.generateCsvFile("************************",timeStamp);
        CreateCsvFile.generateCsvFile("Verify admin approve product  test case Started =="+ TC03_Admin_Approve_Added_Product.class,timeStamp);
      TC02_Admin_Verify_Added_Products.verifyNewlyaddedproducts();
      Thread.sleep(3000);
      adminDashBoardPage.scrollDownInWindow();
      Thread.sleep(3000);
      adminDashBoardPage.clickOnApproveBtn();
      Thread.sleep(5000);
      Assert.assertEquals(adminDashBoardPage.VerifySuccesmsg(),"SUCCESS");
        CreateCsvFile.generateCsvFile("************************",timeStamp);
        CreateCsvFile.generateCsvFile("Verify admin approve product  test case Finished =="+ TC03_Admin_Approve_Added_Product.class,timeStamp);
    }


    @AfterTest
    public  void teardown()
    {
        driver.quit();
    }

}
