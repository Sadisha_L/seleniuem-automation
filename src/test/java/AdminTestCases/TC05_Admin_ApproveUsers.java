package AdminTestCases;

import AdminPages.AdminDashBoardPage;
import AdminPages.AdminOnbordPage;
import BaseClasses.TestBaseAdmin;
import UserTestCases.TC10_Register_UserAs_HelaviruLead;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;



public class TC05_Admin_ApproveUsers  extends TestBaseAdmin {


    final static Logger logger=Logger.getLogger(TC05_Admin_ApproveUsers.class);

    @Parameters({ "mobile_number"})
    @Test(priority = '1')
    public void admin_approve_created_users(final String mobilenumber) throws InterruptedException
    {

        AdminDashBoardPage adminDashBoardPage = PageFactory.initElements(driver, AdminDashBoardPage.class);
        TC01_Admin_LoginTo_System.loginTo_system();
        logger.info("This is info : Admin logged to the system " );
        Thread.sleep(2000);
        AdminOnbordPage onboardpg  =adminDashBoardPage.navigateToOnbordingApproval();
        Thread.sleep(2000);

        onboardpg.searchOnbordingUsers(mobilenumber);
        Thread.sleep(2000);
        onboardpg.clickOnViewButton();
        Thread.sleep(2000);
        onboardpg.clickOnVerifyButton();
        Thread.sleep(3000);
        logger.info("This is info : User approved Successfully " );
        Assert.assertEquals(onboardpg.VerifyUserActivatedSuccesfullyMsg(),"APPROVED");



    }




}
