package AdminTestCases;

import AdminPages.AdminDashBoardPage;
import AdminPages.AdminLoginPage;
import BaseClasses.CreateCsvFile;
import BaseClasses.TestBaseAdmin;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import static AdminTestCases.TC02_Admin_Verify_Added_Products.productName;
import static BaseClasses.TestBase.timeStamp;

public class TC04_Admin_Validate_Product_ISActive extends TestBaseAdmin {

    @Test
    public void product_Is_Active() throws InterruptedException

    {

        AdminDashBoardPage adminDashBoardPage = PageFactory.initElements(driver, AdminDashBoardPage.class);
        CreateCsvFile.generateCsvFile("************************",timeStamp);
        CreateCsvFile.generateCsvFile("Verify  approved product status  test case Started =="+ TC04_Admin_Validate_Product_ISActive.class,timeStamp);
        TC01_Admin_LoginTo_System.loginTo_system();
        Thread.sleep(2000);
        adminDashBoardPage.scrollDownInWindow();
        Thread.sleep(200);
        adminDashBoardPage.clickPrductsmenuItem();
        Thread.sleep(2000);
        adminDashBoardPage.clickOnProductView();
        Thread.sleep(8000);
        adminDashBoardPage.VerifyNewlyaddedPageIsInThere(productName);
        Thread.sleep(2000);
        String productStatus=adminDashBoardPage.selectProductFromTableAsActive(productName);
        System.out.println("Product Status is equal to === "+productStatus);
        Assert.assertEquals(productStatus,"Active");
        CreateCsvFile.generateCsvFile("************************",timeStamp);
        CreateCsvFile.generateCsvFile("Verify  approved product status  test case Finished =="+ TC04_Admin_Validate_Product_ISActive.class,timeStamp);


    }
    @AfterTest
    public  void teardown()
    {
        driver.quit();
    }

}
