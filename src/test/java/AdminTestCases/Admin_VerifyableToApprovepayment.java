package AdminTestCases;

import AdminPages.AdminDashBoardPage;
import BaseClasses.TestBaseAdmin;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class Admin_VerifyableToApprovepayment extends TestBaseAdmin {

    @Test
    public static void admin_ableTo_approve_payment() throws InterruptedException
    {
        AdminDashBoardPage adminDashBoardPage = PageFactory.initElements(driver, AdminDashBoardPage.class);
        Admin_VerifyPaymentRecordISAvilable.verify_payment_Record_is_Available();
        Thread.sleep(2000);
        adminDashBoardPage.clickOnViewBtn();
        Thread.sleep(2000);
        adminDashBoardPage.addPaymentRemark("payment Verified");
        Thread.sleep(2000);
        adminDashBoardPage.clickOnPaymentApproveBtn();
        Thread.sleep(2000);
        Assert.assertEquals(adminDashBoardPage.VerifyApprovals(),"Pending payment approved");
    }




    @AfterTest
    public  void teardown()
    {driver.quit();
}}
