package AdminTestCases;

import AdminPages.AdminDashBoardPage;
import AdminPages.AdminProductPage;
import BaseClasses.CreateCsvFile;
import BaseClasses.TestBaseAdmin;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import static BaseClasses.TestBase.timeStamp;

public class TC02_Admin_Verify_Added_Products extends TestBaseAdmin {

    public static String productName="Test Product By Sadisha";

    @Test
    public  static void verifyNewlyaddedproducts() throws InterruptedException
    {
        AdminDashBoardPage adminDashBoardPage = PageFactory.initElements(driver, AdminDashBoardPage.class);
        CreateCsvFile.generateCsvFile("************************",timeStamp);
        CreateCsvFile.generateCsvFile("Verify newly added product  test case Started =="+ TC02_Admin_Verify_Added_Products.class,timeStamp);
        userlogin();
        Thread.sleep(3000);
        adminDashBoardPage.scrollDownInWindow();
        Thread.sleep(2000);
        adminDashBoardPage.clickPrductsmenuItem();
        Thread.sleep(3000);
        AdminProductPage productPage=adminDashBoardPage.clickOnProductActivationsubmenu();
        Thread.sleep(3000);
        productPage.VerifyNewlyaddedPageIsInThere(productName);
        Thread.sleep(3000);
        String selectedValue=productPage.selectProductFromTable(productName);
        Thread.sleep(2000);
        System.out.println("Selected value from table == "+selectedValue);
        Assert.assertEquals(productPage.verifyProductName(),productName);
        CreateCsvFile.generateCsvFile("************************",timeStamp);
        CreateCsvFile.generateCsvFile("Verify newly added product test case Finished =="+ TC02_Admin_Verify_Added_Products.class,timeStamp);




    }


    public static void userlogin() throws InterruptedException
    {
        TC01_Admin_LoginTo_System bbb=new TC01_Admin_LoginTo_System();
        Thread.sleep(2000);
        bbb.loginTo_system();
    }

    @AfterTest
    public  void teardown()
    {
        driver.quit();
    }

}
