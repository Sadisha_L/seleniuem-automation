package AdminTestCases;

import AdminPages.AdminDashBoardPage;
import BaseClasses.TestBaseAdmin;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class Admin_ValidatePendingPaymentAvailability extends TestBaseAdmin {

    @Test
    public void verify_payment_record_availability() throws InterruptedException
    {
        AdminDashBoardPage adminDashBoardPage = PageFactory.initElements(driver, AdminDashBoardPage.class);
        Admin_VerifyableToApprovepayment.admin_ableTo_approve_payment();
        Thread.sleep(2000);
        adminDashBoardPage.navigateToPaymentVerificationHistory();

    }
}
