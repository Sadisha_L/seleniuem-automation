package UserTestCases;


import BaseClasses.TestBase;
import UserPages.*;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class TC03_Register_UserAs_HelaviruRetailer extends TestBase {


    @Test(priority = '1')
    public void register_new_user2() throws InterruptedException {
        DashBoardPg dashBoardPg = PageFactory.initElements(driver, DashBoardPg.class);
        Thread.sleep(2000);
        dashBoardPg.clickOnRegisterBtn();
        Thread.sleep(5000);
        dashBoardPg.selectIndividualType();
        Thread.sleep(5000);
        RegisterPage regpg=dashBoardPg.selectTypeAsRetailer();
        Thread.sleep(4000);
        regpg.typeFullname("Sadisha LokuYaddehige Helaviru Retailer");
        Thread.sleep(2000);
        regpg.typeNICnumber2();
        Thread.sleep(2000);
        regpg.typeManualMobileNumber("0790000006");
        //regpg.typeMobileNumber2();
        Thread.sleep(2000);
        RegisterPageRetailer regRetailer =regpg.typeAddressLane2("number 15 ,Colombo 7,sri Lanka");
        Thread.sleep(2000);
        regRetailer.clickOnProvinceRetailer("Central");
        Thread.sleep(2000);
        regRetailer.selectDistrictRetailer("Kandy");
        Thread.sleep(2000);
        regRetailer.selectCityRetailer("Akurana");
        Thread.sleep(2000);
        regRetailer.selectDivisionalSecretarantRetailer("Akurana");
        Thread.sleep(2000);
        regRetailer.selectGramaniladariRetailer("Balakaduwa");
        Thread.sleep(2000);
        regRetailer.enterUserNameRetailer();
        Thread.sleep(2000);
        regRetailer.checkNatureOfFruitType();
        Thread.sleep(2000);
        regRetailer.checkExport();
        Thread.sleep(2000);
        regRetailer.schollDownTosignUpBtn();
        Thread.sleep(2000);
        regRetailer.clickverifyAgreementRetiler();
        Thread.sleep(2000);
        regRetailer.clickOnSignINBtn();


    }

    @AfterMethod
    public void enter_retailer_otpcode() throws InterruptedException
    {
        RegisterPageRetailer regRetailer = PageFactory.initElements(driver, RegisterPageRetailer.class);
        OTPInfoPage    otppg           = PageFactory.initElements(driver, OTPInfoPage.class);
        Thread.sleep(2000);
        regRetailer.clickOnOTPinput1_Retailer();
        Thread.sleep(2000);
        regRetailer.enterOTPinput1_Retailer("0");
        Thread.sleep(2000);
        regRetailer.enterOTPinput2_Retailer("0");
        Thread.sleep(2000);
        regRetailer.enterOTPinput3_Retailer("0");
        Thread.sleep(2000);
        regRetailer.enterOTPinput4_Retailer("0");
        Thread.sleep(2000);
        regRetailer.enterOTPinput5_Retailer("0");
        Thread.sleep(2000);
        regRetailer.enterOTPinput6_Retailer("0");
        Thread.sleep(2000);
        regRetailer.clickOnOTPverifyBTN();
        Thread.sleep(5000);
       // Assert.assertEquals(otppg.verifyMessage(),"Thank you for registering with Helaviru. Your account will be reviewed and approved shortly.");

    }







}
