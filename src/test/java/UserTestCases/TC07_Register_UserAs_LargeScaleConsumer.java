package UserTestCases;

import BaseClasses.TestBase;
import UserPages.*;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class TC07_Register_UserAs_LargeScaleConsumer extends TestBase {



    @Test(priority = '1')
    public void register_new_user() throws InterruptedException {

        DashBoardPg dashBoardPg = PageFactory.initElements(driver, DashBoardPg.class);
        RegisterPage regpg =PageFactory.initElements(driver, RegisterPage.class);
        dashBoardPg.clickOnRegisterBtn();
        Thread.sleep(5000);
        dashBoardPg.selectIndividualType();
        Thread.sleep(5000);
        RegisterPageLargeScaleConsumer largeScaleregpg =dashBoardPg.selectTypeAsLargeScaleConsumer();

        Thread.sleep(4000);
        regpg.typeFullname("Sadisha Randika Helaviru Large scale consumer");
        Thread.sleep(2000);
        regpg.typeNicnumber();
        Thread.sleep(2000);
        regpg.typeManualMobileNumber("0790000006");
        Thread.sleep(2000);
        regpg.typeAddressLane1("no 25 ,Abc lane ,Nugegoda,Colombo ");
        Thread.sleep(2000);



        largeScaleregpg.clickOnProvinceLargeScaleConsumer("Central");
        Thread.sleep(2000);
        largeScaleregpg.selectDistrictLargeScaleConsumer("Kandy");
        Thread.sleep(4000);
        largeScaleregpg.selectCityLargeScaleConsumer("Akurana");
        Thread.sleep(4000);
        largeScaleregpg.selectDivisionalSecretarantLargeScaleConsumer("Akurana");
        Thread.sleep(4000);
        largeScaleregpg.selectGramaniladariLargeScaleConsumer("Balakaduwa");
        Thread.sleep(2000);
        regpg.enterUserName();
        Thread.sleep(2000);
        regpg.schollDownTosignUpBtn();
        Thread.sleep(2000);
        largeScaleregpg.clickVerifyLargeScaleConsumerAgreemnt();
        Thread.sleep(2000);
        regpg.clickOnSignINBtn();
        Thread.sleep(5000);




    }
    @AfterMethod
    public  void enter_otp() throws InterruptedException
    {
        RegisterPageLargeScaleConsumer largeScaleregpg = PageFactory.initElements(driver, RegisterPageLargeScaleConsumer.class);
        OTPInfoPage otppg           = PageFactory.initElements(driver, OTPInfoPage.class);
        Thread.sleep(2000);
        largeScaleregpg.clickOnOTPinput1_LargeScaleConsumer();
        Thread.sleep(2000);
        largeScaleregpg.enterOTPinput1_LargeScaleConsumer("0");
        Thread.sleep(2000);
        largeScaleregpg.enterOTPinput2_LargeScaleConsumer("0");
        Thread.sleep(2000);
        largeScaleregpg.enterOTPinput3_LargeScaleConsumer("0");
        Thread.sleep(2000);
        largeScaleregpg.enterOTPinput4_LargeScaleConsumer("0");
        Thread.sleep(2000);
        largeScaleregpg.enterOTPinput5_LargeScaleConsumer("0");
        Thread.sleep(2000);
        largeScaleregpg.enterOTPinput6_LargeScaleConsumer("0");
        Thread.sleep(2000);
        largeScaleregpg.clickOnOTPverifyBTN();
        Thread.sleep(5000);
      //  Assert.assertEquals(otppg.verifyMessage(),"Thank you for registering with Helaviru. Your account will be reviewed and approved shortly.");


    }








}
