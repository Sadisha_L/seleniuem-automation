package UserTestCases;

import BaseClasses.CreateCsvFile;
import BaseClasses.TestBase;
import UserPages.BasePage;
import UserPages.DashBoardPg;
import UserPages.ProductPurchasePage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;




public class ValidateSellerAbleTobuyProductsViaBank extends TestBase {

    public static String abcd;

    @Test
    public void buyProductsByUser() throws InterruptedException {
        CreateCsvFile.generateCsvFile("************************","exCreate");
        CreateCsvFile.generateCsvFile("Test case Started =="+ ValidateSellerAbleTobuyProductsViaBank.class,timeStamp);
        TC01_User_LoginToSystem.testUserLoginwithValidCredintials();
        DashBoardPg dashBoardPg = PageFactory.initElements(driver, DashBoardPg.class);
        Thread.sleep(5000);
        ProductPurchasePage productPurchasePg = dashBoardPg.navigateToLandingUI();
        Thread.sleep(2000);
        productPurchasePg.clickOnBuyBtn();
        Thread.sleep(2000);
        productPurchasePg.selectBuyingTypeAsSPOT();
        Thread.sleep(5000);
        abcd = productPurchasePg.calculateNumberOfProducts();
        Thread.sleep(3000);
        String value =productPurchasePg.test();
        System.out.println("Filtered product count is equal to ==  "+value);
        productPurchasePg.selectProductTopurchase(value);
        Thread.sleep(3000);
        productPurchasePg.selectProduct(value);
        Thread.sleep(5000);
        productPurchasePg.clickOnProductBuyBtn();
        Thread.sleep(4000);
        productPurchasePg.selectTransporttype();
        Thread.sleep(2000);
        productPurchasePg.clickOnDeliveryProceedBtn();
        Thread.sleep(2000);
        BasePage.UI_schollDown();
        Thread.sleep(2000);
        productPurchasePg.scrollDownToTypeBankTrnsfer();
        Thread.sleep(2000);
        productPurchasePg.selectPaymentTypeAsBankTransfer();
        Thread.sleep(3000);
        productPurchasePg.selectTypeAsCashDeposit();
        Thread.sleep(3000);
        productPurchasePg.clickOnBankTransferProceedBtn();
        Thread.sleep(2000);
        productPurchasePg.addReferenceNumberToPayment("4355667765");
        Thread.sleep(4000);
        productPurchasePg.clickOnReferenceDoneBtn();
        Thread.sleep(5000);
       /* String orderNumber   =productPurchasePg.getOrderIdnumber();
        System.out.println("Order number is == "+orderNumber);*/


    }








}

