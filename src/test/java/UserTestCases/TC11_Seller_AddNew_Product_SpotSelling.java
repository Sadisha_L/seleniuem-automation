package UserTestCases;

import BaseClasses.TestBase;
import UserPages.OverviewPage;
import UserPages.ProductPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class TC11_Seller_AddNew_Product_SpotSelling extends TestBase {

    /* Created By sadisha
     This Test case is design for create a new spot selling product ,System bug is there unable to complete  */

    @Test
    public void add_spot_selling_product() throws InterruptedException {
        OverviewPage overviewPage = PageFactory.initElements(driver, OverviewPage.class);
        Navigate_To_MyAccount.navigate_to_overview();
        Thread.sleep(1000);
        ProductPage productpg = overviewPage.clickOnSellProductBtn();
        Thread.sleep(5000);
        productpg.clickonSpotSellingBtn();
        Thread.sleep(7000);
        productpg.selectMainCategory("Vegetables");
        Thread.sleep(3000);
        productpg.selectSubCategory("Beans");
        Thread.sleep(2000);
        productpg.clickOn_addNewProduct_nextBtn();
        Thread.sleep(4000);
       /* productpg.addtitletoProduct("Test product");
        Thread.sleep(2000);
        productpg.addDescription("Sample Product description");
        Thread.sleep(2000);*/
        productpg.clickOnCalander();
        Thread.sleep(2000);
      // productpg.clickOnYearDropdown();

    }




}
