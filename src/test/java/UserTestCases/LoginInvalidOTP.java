package UserTestCases;

import BaseClasses.TestBase;
import UserPages.LoginPg;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class LoginInvalidOTP extends TestBase {




   @Test
    public void testLoginWithWrongOtp() throws InterruptedException {
        LoginPg loginPage = PageFactory.initElements(driver, LoginPg.class);
        loginPage.clickLoginLink();
        Thread.sleep(2000);
        loginPage.clickUserName();
        Thread.sleep(2000);
        loginPage.typeUserName("sadishar");
        Thread.sleep(2000);
        loginPage.clickLoginButton();
        Thread.sleep(2000);
        loginPage.clickOtpField1();
        Thread.sleep(2000);
        loginPage .typePasswordField1("1");
        Thread.sleep(2000);
        loginPage .typePasswordField2("2");
        Thread.sleep(2000);
        loginPage.typePasswordField3("3");
        Thread.sleep(2000);
        loginPage.typePasswordField4("3");
        Thread.sleep(2000);
        loginPage.typePasswordField5("3");
        Thread.sleep(2000);
        loginPage.typePasswordField6("3");
        Thread.sleep(2000);
        loginPage.clickVerifyBtn();
        Thread.sleep(2000);
        String aaa =loginPage.getErrorMessage();
        System.out.println("value of the message is " +aaa);
        Assert.assertEquals(loginPage.getErrorMessage(), "Invalid OTP!");
        Thread.sleep(5000);
    }


    @AfterTest
    public  void teardown()
    {
        driver.quit();
    }





}
