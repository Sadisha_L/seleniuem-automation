package UserTestCases;

import BaseClasses.TestBase;
import UserPages.OverviewPage;
import UserPages.ProductPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.*;

public class TC12_Seller_AddNew_Product_Auction_Selling extends TestBase {

    /* Created By sadisha
     This Test case is design for create a new Auction selling product */

    @Test
    public void add_auction_selling_product() throws InterruptedException, AWTException {
        OverviewPage overviewPage = PageFactory.initElements(driver, OverviewPage.class);
        Navigate_To_MyAccount.navigate_to_overview();
        Thread.sleep(3000);
        ProductPage productpg = overviewPage.clickOnSellProductBtn();
        Thread.sleep(5000);
        productpg.selectTypeAsAution();
        Thread.sleep(7000);
        productpg.selectMainCategoryAuction("Vegetables");
        Thread.sleep(3000);
        productpg.selectSubCategoryAuction("Beans");
        Thread.sleep(4000);
        productpg.addAutionPeriedToDate("20-02-2022");
        Thread.sleep(4000);
        productpg.enterMinimumBidValue("50000");
        Thread.sleep(2000);
        productpg.clickOn_addNewProduct_nextBtn();
        Thread.sleep(4000);
        productpg.addtitleToproduct("Sadisha Test Product");
        Thread.sleep(4000);
        productpg.addDescription("This is Sample Product for Testing ");
        Thread.sleep(4000);
        productpg.setExpiarydatesToProduct("22-02-2025");
        Thread.sleep(4000);
        productpg.availableTotalQuentity("100");
        Thread.sleep(2000);
        productpg.selectUnitType("Kg");
        Thread.sleep(5000);
        productpg.clickONNextBtnInAddressVerify();
        Thread.sleep(2000);
        productpg.ClickOnAddressVerifypageNxtBtn();
        Thread.sleep(4000);
        productpg.selectDelivaryOptnSelf("5000");
        Thread.sleep(2000);
        productpg.clickonDelivaryRateNextBtn();
        Thread.sleep(2000);
        productpg.clickOnFileAddButton("C:\\Users\\sadisha\\IdeaProjects\\HelaviruFrameWork\\src\\test\\Utill\\images\\download1mb.jpg");
        Thread.sleep(2000);
        productpg.fileuploadNextBtn();
        Thread.sleep(7000);
        Assert.assertEquals
                (productpg.verifyProductCreatedSuccesfully(), "Product uploaded successfully! Your product will be available in the market shortly, once the review is completed." );


    }




}
