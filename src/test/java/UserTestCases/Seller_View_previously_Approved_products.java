package UserTestCases;

import BaseClasses.TestBase;

import UserPages.OverviewPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class Seller_View_previously_Approved_products extends TestBase {

    @Test
    public void viewAddedProductsBySeller() throws InterruptedException
    {

        Navigate_To_MyAccount.navigate_to_overview();
        OverviewPage overviewPage = PageFactory.initElements(driver, OverviewPage.class);
        Thread.sleep(5000);
        overviewPage.navigateToMyProduct();
        Thread.sleep(3000);
        overviewPage.selectValuesfromProductspage("");
    }

}
