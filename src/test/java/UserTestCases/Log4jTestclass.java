package UserTestCases;

import BaseClasses.TestBase;
import UserPages.LoginPg;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import org.apache.log4j.Logger;


public  class Log4jTestclass extends TestBase {


    final static Logger logger=Logger.getLogger(Log4jTestclass.class);

    @Test
    public  void login() throws InterruptedException {

        LoginPg loginPage = PageFactory.initElements(driver, LoginPg.class);
        Thread.sleep(2000);

        loginPage.clickLoginLink();
        logger.debug("This is debug");
        Thread.sleep(2000);
        loginPage.clickUserName();
        logger.info("This is info : " );

        logger.debug("This is debug file 2 ");
        Thread.sleep(2000);
        loginPage.typeUserName("sadishe");
        Thread.sleep(2000);
        loginPage.clickLoginButton();

    }



}
