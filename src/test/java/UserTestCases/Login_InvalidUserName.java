package UserTestCases;

import BaseClasses.CreateCsvFile;
import BaseClasses.TestBase;
import UserPages.LoginPg;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class Login_InvalidUserName extends TestBase {

    @Test(priority = 1)
    public  void testUserLoginwithInvalidUsername() throws InterruptedException
    {
        LoginPg loginPage = PageFactory.initElements(driver, LoginPg.class);
        CreateCsvFile.generateCsvFile("Invalid login started successfully ==","exCreate");
        CreateCsvFile.generateCsvFile("Invalid login started successfully =="+this.getClass().getName(),"exCreate");
        loginPage.clickLoginLink();
        Thread.sleep(2000);
        loginPage.clickUserName();
        Thread.sleep(3000);
        loginPage.typeUserName("aaaaa");
        Thread.sleep(3000);
        loginPage.clickLoginButton();
        Thread.sleep(2000);
        String aaa =loginPage.getErrorMessage2();
        System.out.println("value of the message is " +aaa);
        Assert.assertEquals(loginPage.getErrorMessage2(), "Invalid username!");
        CreateCsvFile.generateCsvFile("Invalid login ended successfully == ","exCreate");
        }
 @AfterTest
    public  void teardown()
{driver.quit();
}




}
