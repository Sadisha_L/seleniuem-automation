package UserTestCases;

import BaseClasses.TestBase;
import UserPages.DashBoardPg;
import UserPages.OTPInfoPage;
import UserPages.RegisterPage;
import UserPages.RegisterPageAgroProduce;
import org.openqa.selenium.support.PageFactory;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class TC05_Register_UserAs_Agro_Produce_Aggregator extends TestBase {


    @Test(priority = '1')
    public void register_new_user() throws InterruptedException {

        DashBoardPg dashBoardPg = PageFactory.initElements(driver, DashBoardPg.class);
        RegisterPage regpg =PageFactory.initElements(driver, RegisterPage.class);
        dashBoardPg.clickOnRegisterBtn();
        Thread.sleep(5000);
        dashBoardPg.selectIndividualType();
        Thread.sleep(5000);
        RegisterPageAgroProduce agroProduceregpg =dashBoardPg.selectTypeAsAgroProduce();
        Thread.sleep(4000);
        regpg.typeFullname("Sadisha Randika Helaviru Agro Producer");
        Thread.sleep(2000);
        regpg.typeNicnumber();
        Thread.sleep(2000);
        regpg.typeManualMobileNumber("0790000006");
        Thread.sleep(2000);
        regpg.typeAddressLane1("no 25 ,Abc lane ,Nugegoda");
        Thread.sleep(2000);
        agroProduceregpg.clickOnProvinceAgroProducer("Central");
        Thread.sleep(2000);
        agroProduceregpg.selectDistrictAgroProducer("Kandy");
        Thread.sleep(4000);
        agroProduceregpg.selectCityAgroProducer("Akurana");
        Thread.sleep(4000);
        agroProduceregpg.selectDivisionalSecretarantAgroProducer("Akurana");
        Thread.sleep(4000);
        agroProduceregpg.selectGramaniladariAgroProducer("Balakaduwa");
        Thread.sleep(2000);
        regpg.enterUserName();
        Thread.sleep(2000);
        regpg.schollDownTosignUpBtn();
        Thread.sleep(2000);
        agroProduceregpg.clickVerifyAgroProducerAgreemnt();
        Thread.sleep(2000);
        regpg.clickOnSignINBtn();
        Thread.sleep(5000);




    }
    @AfterMethod
    public  void enter_otp() throws InterruptedException
    {
        RegisterPageAgroProduce agroProduceregpg = PageFactory.initElements(driver, RegisterPageAgroProduce.class);
        OTPInfoPage otppg           = PageFactory.initElements(driver, OTPInfoPage.class);
        Thread.sleep(2000);
        agroProduceregpg.clickOnOTPinput1_agroProduce();
        Thread.sleep(2000);
        agroProduceregpg.enterOTPinput1_agroProduce("0");
        Thread.sleep(2000);
        agroProduceregpg.enterOTPinput2_agroProduce("0");
        Thread.sleep(2000);
        agroProduceregpg.enterOTPinput3_agroProduce("0");
        Thread.sleep(2000);
        agroProduceregpg.enterOTPinput4_agroProduce("0");
        Thread.sleep(2000);
        agroProduceregpg.enterOTPinput5_agroProduce("0");
        Thread.sleep(2000);
        agroProduceregpg.enterOTPinput6_agroProduce("0");
        Thread.sleep(2000);
        agroProduceregpg.clickOnOTPverifyBTN();
        Thread.sleep(5000);
       // Assert.assertEquals(otppg.verifyMessage(),"Thank you for registering with Helaviru. Your account will be reviewed and approved shortly.");


    }





}
