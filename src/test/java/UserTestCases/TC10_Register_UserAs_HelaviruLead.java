package UserTestCases;

import BaseClasses.TestBase;
import UserPages.DashBoardPg;
import UserPages.OTPInfoPage;
import UserPages.RegisterPage;
import UserPages.RegisterPageLead;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class TC10_Register_UserAs_HelaviruLead extends TestBase {

    /* Created By sadisha
        */

    final static Logger logger=Logger.getLogger(TC10_Register_UserAs_HelaviruLead.class);

    @Test(priority = '1')
    public void register_new_user_HelaviruLead() throws InterruptedException {

        DashBoardPg dashBoardPg = PageFactory.initElements(driver, DashBoardPg.class);
        RegisterPage regpg =PageFactory.initElements(driver, RegisterPage.class);
        dashBoardPg.clickOnRegisterBtn();
        Thread.sleep(5000);
        dashBoardPg.selectIndividualType();
        logger.info("This is info : user type is individual " );
        Thread.sleep(5000);
        RegisterPageLead leadregpg =dashBoardPg.selectTypeAsHelaviruLead();
        logger.info("This is info : user type selected as Helaviru Lead" );
        Thread.sleep(2000);
        regpg.scrollToUp();
        Thread.sleep(2000);
        leadregpg.enterName("Sadisha Randika Helaviru Lead User");
        logger.info("This is info : entered helaviru lead name  " );

        Thread.sleep(2000);
        regpg.typeNicnumber();
        logger.info("This is info : " );
        Thread.sleep(2000);
        regpg.typeManualMobileNumber("0790000005");
        Thread.sleep(2000);
        regpg.typeAddressLane1("no 25 ,Abc lane ,Nugegoda,Colombo ");
        Thread.sleep(2000);



        leadregpg.clickOnProvinceLead("Central");
        Thread.sleep(2000);
        leadregpg.selectDistrictLead("Kandy");
        Thread.sleep(4000);
        leadregpg.selectCityLead("Akurana");
        Thread.sleep(4000);
        leadregpg.selectDivisionalSecretarantLead("Akurana");
        Thread.sleep(4000);
        leadregpg.selectGramaniladariLead("Balakaduwa");
        Thread.sleep(2000);
        regpg.enterUserName();
        Thread.sleep(2000);

        Thread.sleep(2000);

        leadregpg.verifyExtraActivities();
        Thread.sleep(1000);
        leadregpg.verifyexams();
        Thread.sleep(1000);
        regpg.schollDownTosignUpBtn();
       // leadregpg.serviceTypes();
        leadregpg.clickVerifyLead();
        Thread.sleep(2000);
        regpg.clickOnSignINBtn();
        Thread.sleep(5000);
        logger.info("This is info : sign up button executed " );





    }
    @AfterMethod
    public  void enter_otp() throws InterruptedException
    {
        RegisterPageLead leadregpg = PageFactory.initElements(driver, RegisterPageLead.class);
        OTPInfoPage otppg           = PageFactory.initElements(driver, OTPInfoPage.class);
        Thread.sleep(2000);
        leadregpg.clickOnOTPinput1_Lead();
        Thread.sleep(2000);
        leadregpg.enterOTPinput1_Lead("0");
        Thread.sleep(2000);
        leadregpg.enterOTPinput2_Lead("0");
        Thread.sleep(2000);
        leadregpg.enterOTPinput3_Lead("0");
        Thread.sleep(2000);
        leadregpg.enterOTPinput4_Lead("0");
        Thread.sleep(2000);
        leadregpg.enterOTPinput5_Lead("0");
        Thread.sleep(2000);
        leadregpg.enterOTPinput6_Lead("0");
        Thread.sleep(2000);
        leadregpg.clickOnVerifyBtn();
        logger.info("This is info : OTP verify button executed sucsesfully " );
        // Assert.assertEquals(otppg.verifyMessage(),"Thank you for registering with Helaviru. Your account will be reviewed and approved shortly.");


    }
}
