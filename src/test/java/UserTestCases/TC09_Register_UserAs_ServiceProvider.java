package UserTestCases;

import BaseClasses.TestBase;
import UserPages.DashBoardPg;
import UserPages.OTPInfoPage;
import UserPages.RegisterPage;
import UserPages.RegisterPageServiceprovider;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class TC09_Register_UserAs_ServiceProvider extends TestBase {



    @Test(priority = '1')
    public void register_new_user_ServiceProvider() throws InterruptedException {

        DashBoardPg dashBoardPg = PageFactory.initElements(driver, DashBoardPg.class);
        RegisterPage regpg =PageFactory.initElements(driver, RegisterPage.class);
        dashBoardPg.clickOnRegisterBtn();
        Thread.sleep(5000);
        dashBoardPg.selectIndividualType();
        Thread.sleep(5000);
        RegisterPageServiceprovider serviceregpg =dashBoardPg.selectTypeAsServiceProvider();
        Thread.sleep(2000);
        regpg.scrollToUp();
        Thread.sleep(2000);
        serviceregpg.enterName("Sadisha Randika Helaviru Service Provider ");

        Thread.sleep(2000);
        regpg.typeNicnumber();
        Thread.sleep(2000);
        regpg.typeManualMobileNumber("0790000006");
        Thread.sleep(2000);
        regpg.typeAddressLane1("no 25 ,Abc lane ,Nugegoda,Colombo ");
        Thread.sleep(2000);



        serviceregpg.clickOnProvinceServiceProvider("Central");
        Thread.sleep(2000);
        serviceregpg.selectDistrictServiceProviderr("Kandy");
        Thread.sleep(4000);
        serviceregpg.selectCityServiceProvider("Akurana");
        Thread.sleep(4000);
        serviceregpg.selectDivisionalSecretarantServiceProvider("Akurana");
        Thread.sleep(4000);
        serviceregpg.selectGramaniladariServiceProvider("Balakaduwa");
        Thread.sleep(2000);
        regpg.enterUserName();
        Thread.sleep(2000);
        regpg.schollDownTosignUpBtn();
        Thread.sleep(2000);
        serviceregpg.serviceTypes();
        serviceregpg.clickVerifyServiceProvider();
        Thread.sleep(2000);
        regpg.clickOnSignINBtn();
        Thread.sleep(5000);




    }
    @AfterMethod
    public  void enter_otp() throws InterruptedException
    {
        RegisterPageServiceprovider serviceregpg = PageFactory.initElements(driver, RegisterPageServiceprovider.class);
        OTPInfoPage otppg           = PageFactory.initElements(driver, OTPInfoPage.class);
        Thread.sleep(2000);
        serviceregpg.clickOnOTPinput1_ServiceProvider();
        Thread.sleep(2000);
        serviceregpg.enterOTPinput1_ServiceProvider("0");
        Thread.sleep(2000);
        serviceregpg.enterOTPinput2_ServiceProvider("0");
        Thread.sleep(2000);
        serviceregpg.enterOTPinput3_ServiceProvider("0");
        Thread.sleep(2000);
        serviceregpg.enterOTPinput4_ServiceProvider("0");
        Thread.sleep(2000);
        serviceregpg.enterOTPinput5_ServiceProvider("0");
        Thread.sleep(2000);
        serviceregpg.enterOTPinput6_ServiceProvider("0");
        Thread.sleep(2000);
        serviceregpg.clickOnOTPverifyBTN();
        Thread.sleep(5000);
       // Assert.assertEquals(otppg.verifyMessage(),"Thank you for registering with Helaviru. Your account will be reviewed and approved shortly.");


    }
}
