package UserTestCases;

import BaseClasses.TestBase;
import UserPages.DashBoardPg;
import UserPages.OTPInfoPage;
import UserPages.RegisterPage;
import UserPages.RegisterPageWholesaler;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class TC06_Register_UserAs_Wholesaler extends TestBase {


    @Test(priority = '1')
    public void register_new_user() throws InterruptedException {

        DashBoardPg dashBoardPg = PageFactory.initElements(driver, DashBoardPg.class);
        RegisterPage regpg =PageFactory.initElements(driver, RegisterPage.class);
        dashBoardPg.clickOnRegisterBtn();
        Thread.sleep(5000);
        dashBoardPg.selectIndividualType();
        Thread.sleep(5000);
        RegisterPageWholesaler wholesalerregpg =dashBoardPg.selectTypeAsWholesaler();

        Thread.sleep(4000);
        regpg.typeFullname("Sadisha Randika Helaviru Whole Saler");
        Thread.sleep(2000);
        regpg.typeNicnumber();
        Thread.sleep(2000);
        regpg.typeManualMobileNumber("0790000006");
        Thread.sleep(2000);
        regpg.typeAddressLane1("no 25 ,Abc lane ,Nugegoda");
        Thread.sleep(2000);
        wholesalerregpg.clickOnProvinceWholesaler("Central");
        Thread.sleep(2000);
        wholesalerregpg.selectDistrictWholesaler("Kandy");
        Thread.sleep(4000);
        wholesalerregpg.selectCityAgroWholesaler("Akurana");
        Thread.sleep(4000);
        wholesalerregpg.selectDivisionalSecretarantWholesaler("Akurana");
        Thread.sleep(4000);
        wholesalerregpg.selectGramaniladariWholesaler("Balakaduwa");
        Thread.sleep(2000);
        regpg.enterUserName();
        Thread.sleep(2000);
        regpg.schollDownTosignUpBtn();
        Thread.sleep(2000);
        wholesalerregpg.clickVerifyWholesalerAgreemnt();
        Thread.sleep(2000);
        regpg.clickOnSignINBtn();
        Thread.sleep(5000);




    }
    @AfterMethod
    public  void enter_otp() throws InterruptedException
    {
        RegisterPageWholesaler wholesalerregpg = PageFactory.initElements(driver, RegisterPageWholesaler.class);
        OTPInfoPage otppg           = PageFactory.initElements(driver, OTPInfoPage.class);
        Thread.sleep(2000);
        wholesalerregpg.clickOnOTPinput1_Wholesaler();
        Thread.sleep(2000);
        wholesalerregpg.enterOTPinput1_Wholesaler("0");
        Thread.sleep(2000);
        wholesalerregpg.enterOTPinput2_Wholesaler("0");
        Thread.sleep(2000);
        wholesalerregpg.enterOTPinput3_Wholesaler("0");
        Thread.sleep(2000);
        wholesalerregpg.enterOTPinput4_Wholesaler("0");
        Thread.sleep(2000);
        wholesalerregpg.enterOTPinput5_Wholesaler("0");
        Thread.sleep(2000);
        wholesalerregpg.enterOTPinput6_Wholesaler("0");
        Thread.sleep(2000);
        wholesalerregpg.clickOnOTPverifyBTN();
        Thread.sleep(5000);
       // Assert.assertEquals(otppg.verifyMessage(),"Thank you for registering with Helaviru. Your account will be reviewed and approved shortly.");


    }





}
