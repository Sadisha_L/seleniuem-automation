package UserTestCases;

import BaseClasses.TestBase;

import UserPages.DashBoardPg;
import UserPages.OverviewPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;


public class Navigate_To_MyAccount extends TestBase
{

  @Test
   public static void navigate_to_overview() throws InterruptedException
   {
    login_To_System_As_Seller();
    Thread.sleep(2000);
    DashBoardPg dashBoardPg = PageFactory.initElements(driver, DashBoardPg.class);
    OverviewPage overviewPg =dashBoardPg.clickOnMyAccountBtn();
    Thread.sleep(2000);
    Assert.assertEquals(overviewPg.VerifyNavigationToOverviewPage(), "Overview");

   }



    public static void login_To_System_As_Seller() throws InterruptedException
    {
        TC01_User_LoginToSystem bbb=new TC01_User_LoginToSystem();
        Thread.sleep(2000);
        bbb.testUserLoginwithValidCredintials();
    }


    public static void login_To_System_As_buyer() throws InterruptedException
    {
        TC01_SellerUser_LoginToSystem  ddd=new TC01_SellerUser_LoginToSystem();
        Thread.sleep(2000);
                ddd.SellerUserLoginwithValidCredintials();
    }

}
