package UserTestCases;

import BaseClasses.CreateCsvFile;
import BaseClasses.TestBase;
import UserPages.DashBoardPg;
import UserPages.ProductPurchasePage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;


public class ValidateSellerAbleTobuyProductsViaOnline extends TestBase {

    public static String abcd;

    @Test
    public void buyProductsByUser() throws InterruptedException {
        CreateCsvFile.generateCsvFile("************************","exCreate");
        CreateCsvFile.generateCsvFile("Test case Started =="+ ValidateSellerAbleTobuyProductsViaOnline.class,timeStamp);
        TC01_User_LoginToSystem.testUserLoginwithValidCredintials();
        DashBoardPg dashBoardPg = PageFactory.initElements(driver, DashBoardPg.class);
        Thread.sleep(5000);
        ProductPurchasePage productPurchasePg = dashBoardPg.navigateToLandingUI();
        Thread.sleep(2000);
        productPurchasePg.clickOnBuyBtn();
        Thread.sleep(2000);
        productPurchasePg.selectBuyingTypeAsSPOT();
        Thread.sleep(5000);
        abcd = productPurchasePg.calculateNumberOfProducts();
        Thread.sleep(3000);
        productPurchasePg.selectProduct(abcd);
        Thread.sleep(5000);
        productPurchasePg.clickOnProductBuyBtn();
        Thread.sleep(4000);
        productPurchasePg.selectTransporttype();
        Thread.sleep(2000);
        productPurchasePg.clickOnProceedBtn();
        Thread.sleep(9000);
        productPurchasePg.clickOnPayNowBtn();
        Thread.sleep(8000);
        productPurchasePg.clickOnPaymentConfirmBtn();
        Thread.sleep(2000);
        productPurchasePg.entermasterCardNumber("4005555555000009");
        Thread.sleep(2000);
        productPurchasePg.setExpiaryMonth("05");
        Thread.sleep(2000);
        productPurchasePg.setExpiaryYear("25");
        Thread.sleep(2000);
        productPurchasePg.enterCardHolderName("Sadisha Randika");
        Thread.sleep(2000);
        productPurchasePg.enterSecurityCode("000");
        Thread.sleep(2000);
        productPurchasePg.clickonPayNOWBtn();
        Thread.sleep(5000);
        productPurchasePg.clickonACSEmulatersubmitBtn();
        CreateCsvFile.generateCsvFile("Test case ended Successfully  =="+ ValidateSellerAbleTobuyProductsViaOnline.class,"exCreate");
        CreateCsvFile.generateCsvFile("************************","exCreate");

    }








}

