package UserTestCases;

import BaseClasses.TestBase;

import UserPages.MypurchasePage;
import UserPages.OverviewPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class TC16_Validate_Buyer_Order_Visibility_to_Seller extends TestBase {
/*     Created by Sadisha   */

    final static Logger logger=Logger.getLogger(TC16_Validate_Buyer_Order_Visibility_to_Seller.class);
    @Test
    public  void navigate_to_orders() throws InterruptedException, IOException {
        OverviewPage overviewPg= PageFactory.initElements(driver, OverviewPage.class);
        Navigate_To_MyAccount.navigate_to_overview();
        logger.info("This is info : buyer successfully navigated to overview page  " );
        Thread.sleep(3000);
        overviewPg.scrollDown_To_Element();
        Thread.sleep(1000);
        MypurchasePage mypurPg =overviewPg.click_buyingMypurchases_MenuItem();
        logger.info("This is info : Navigated to My Purchase UI   " );
        Thread.sleep(7000);
        mypurPg.clickon_Search_InputField();
        logger.info("This is info : Click on Search input field     " );
        Thread.sleep(2000);
        mypurPg.search_ProductBy_orederId("FDCAC343D6 ");
        Thread.sleep(2000);
        mypurPg.selectOrderOnOrderTable("FDCAC343D6");
        Thread.sleep(2000);
        mypurPg.VerifyProductContent("FDCAC343D6");
        mypurPg.capture_ScreenShot();
        logger.info("This is info : Screen shot captured successfully      " );
        Assert.assertEquals(mypurPg.VerifyProductContent("FDCAC343D6"),"true");
        logger.info("This is info : Order Status captured     " );
        Thread.sleep(2000);

    }
}
