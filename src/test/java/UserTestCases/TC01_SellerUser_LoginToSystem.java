package UserTestCases;

import BaseClasses.TestBase;
import UserPages.DashBoardPg;
import UserPages.LoginPg;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TC01_SellerUser_LoginToSystem extends TestBase {


    @Test
    public static   void SellerUserLoginwithValidCredintials() throws InterruptedException
    {
        LoginPg loginPage = PageFactory.initElements(driver, LoginPg.class);
        Thread.sleep(2000);
        loginPage.clickLoginLink();
        Thread.sleep(2000);
        loginPage.clickUserName();
        Thread.sleep(2000);
        loginPage.typeUserName("sadishe13");
        Thread.sleep(2000);
        loginPage.clickLoginButton();
        Thread.sleep(2000);
        loginPage.clickOtpField1();
        Thread.sleep(2000);
        loginPage .typePasswordField1("0");
        Thread.sleep(2000);
        loginPage .typePasswordField2("0");
        Thread.sleep(2000);
        loginPage.typePasswordField3("0");
        Thread.sleep(2000);
        loginPage.typePasswordField4("0");
        Thread.sleep(2000);
        loginPage.typePasswordField5("0");
        Thread.sleep(2000);
        loginPage.typePasswordField6("0");
        Thread.sleep(2000);
        loginPage.clickVerifyBtn();
        Thread.sleep(6000);
        DashBoardPg dashBoardPg =loginPage.navigateToDashBoardPg();
        Thread.sleep(7000);
        String aaa  =dashBoardPg.VerifyLandingPg();
        Thread.sleep(4000);
        System.out.println(" verify  user navigated to home page ="+aaa);
        Assert.assertEquals(dashBoardPg.VerifyLandingPg(), "My Account");


    }



}
