package UserTestCases;

import BaseClasses.TestBase;
import UserPages.DashBoardPg;
import UserPages.MyAccountPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;



/* Created By sadisha
This test case is designed to validate placed bits by the Seller  User should be retailer user */


public class TC15_Validte_BitRequest_From_Seller extends TestBase {

    @Test
    public void verify_bit_requests_byBuyerUser() throws InterruptedException
    {
        LogintoSystemAsSellerUser();
        Thread.sleep(1000);
        DashBoardPg dashBoardPg = PageFactory.initElements(driver, DashBoardPg.class);
        MyAccountPage myaccobj =dashBoardPg.clickOnMyAccountBtnSellerUser();
        Thread.sleep(6000);
        myaccobj.navigateToMyBidsUI();
        Thread.sleep(6000);
        myaccobj.searchForPlacedBids(" Auction Product 028 ");
        Thread.sleep(2000);
        String value2= myaccobj.verifyBitIsavailable("Auction Product 028");
        System.out.println("returned value from data table as bids === " +value2);
        Thread.sleep(2000);
        Assert.assertEquals(value2, " Auction Product 028 ");


    }


    public static void LogintoSystemAsSellerUser() throws InterruptedException
    {
        TC01_SellerUser_LoginToSystem object1=new TC01_SellerUser_LoginToSystem();
        Thread.sleep(2000);
        object1.SellerUserLoginwithValidCredintials();
    }


}
