package UserTestCases;

import BaseClasses.DataProviders;
import BaseClasses.TestBase;
import UserPages.*;
import jdk.internal.dynalink.beans.StaticClass;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.IOException;

public class TC02_Register_UserAs_HelaviruFarmer extends TestBase {



    @DataProvider(name = "tc_01")
    public Object[][] dataTable_tc_01() throws IOException {


        return DataProviders.readExcel("tc_06");
    }

    @Parameters({ "mobile_number"})
    @Test(priority = '1')
    public void register_new_user(final String mobilenumber) throws InterruptedException {

        DashBoardPg dashBoardPg = PageFactory.initElements(driver, DashBoardPg.class);
        dashBoardPg.clickOnRegisterBtn();
        Thread.sleep(5000);
        dashBoardPg.selectIndividualType();
        Thread.sleep(5000);
        RegisterPage regpg =dashBoardPg.selectTypeAsHelaviruFarmer();
        Thread.sleep(4000);
        regpg.typeFullname("Sadisha Randika Farmer");
        Thread.sleep(2000);
        regpg.typeNicnumber();
        Thread.sleep(2000);
        regpg.typeManualMobileNumber(mobilenumber);
        Thread.sleep(2000);
        RegisterPageFarmer farmerregpg =regpg.typeAddressLane1("no 25 ,Abc lane ,Nugegoda");
        Thread.sleep(2000);
        farmerregpg.clickOnProvinceFarmer();
        Thread.sleep(2000);
        farmerregpg.selectProvinceFarmer("Central");
        Thread.sleep(2000);
        farmerregpg.selectDistrictFarmer("Kandy");
        Thread.sleep(4000);
        farmerregpg.selectCityFarmer("Akurana");
        Thread.sleep(2000);
        farmerregpg.selectDivisionalSecretarantFarmer("Akurana");
        Thread.sleep(2000);
        farmerregpg.selectGramaniladariFarmer("Balakaduwa");
        Thread.sleep(2000);
        farmerregpg.enterUsernameFarmer();
        Thread.sleep(2000);
        farmerregpg.selectTypeAsMahaweliFarmer();
        Thread.sleep(2000);
        farmerregpg.selectWaterResorceTypeAsNatural();
        Thread.sleep(2000);
        farmerregpg.selectCultivatedCrops();
        Thread.sleep(2000);
        farmerregpg.chollDownTosignUpBtn();
        Thread.sleep(2000);
        farmerregpg.clickverifyAgreement();
        Thread.sleep(2000);
        farmerregpg.clickOnSignINBtn();
        Thread.sleep(2000);



    }



    @AfterMethod
    public static void enter_otp() throws InterruptedException
    {
        RegisterPageFarmer farmerregpg = PageFactory.initElements(driver, RegisterPageFarmer.class);
        OTPInfoPage    otppg           = PageFactory.initElements(driver, OTPInfoPage.class);
        Thread.sleep(2000);
        farmerregpg.clickOnOTPinput1_Retailer();
        Thread.sleep(2000);
        farmerregpg.enterOTPinput1_Retailer("0");
        Thread.sleep(2000);
        farmerregpg.enterOTPinput2_Retailer("0");
        Thread.sleep(2000);
        farmerregpg.enterOTPinput3_Retailer("0");
        Thread.sleep(2000);
        farmerregpg.enterOTPinput4_Retailer("0");
        Thread.sleep(2000);
        farmerregpg.enterOTPinput5_Retailer("0");
        Thread.sleep(2000);
        farmerregpg.enterOTPinput6_Retailer("0");
        Thread.sleep(1000);
        farmerregpg.clickOnOTPverifyBTN();
        Thread.sleep(7000);
       // Assert.assertEquals(otppg.verifyMessage(),"Thank you for registering with Helaviru. Your account will be reviewed and approved shortly.");
    }






}
