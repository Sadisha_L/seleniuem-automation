package UserTestCases;

import BaseClasses.TestBase;
import UserPages.DashBoardPg;
import UserPages.MybidsPage;
import UserPages.OverviewPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TC17_Validate_ChangedStatus_of_Awarded_BuyerBid extends TestBase {

    final static Logger logger=Logger.getLogger(TC17_Validate_ChangedStatus_of_Awarded_BuyerBid.class);

    @Test
    public  void verify_status_of_bid() throws InterruptedException {
        DashBoardPg dashBoardPg = PageFactory.initElements(driver, DashBoardPg.class);
        OverviewPage overviewPg= PageFactory.initElements(driver, OverviewPage.class);
        Navigate_To_MyAccount.login_To_System_As_buyer();
        Thread.sleep(3000);
        dashBoardPg.clickOnMyAccountBtn();
        Thread.sleep(5000);
        MybidsPage mybidspg  =overviewPg.clickOnViewmyBidsBtn();
        Thread.sleep(5000);
        mybidspg.searchforBId();
        Thread.sleep(3000);
        mybidspg.searchforBiddedProduct("Sadisha Test Product");
        Thread.sleep(3000);
        String productStatus =mybidspg.verifybidfromTable("Sadisha Test Product");
        Assert.assertEquals(productStatus, "Checkout");


    }
}
