package UserTestCases;

import BaseClasses.TestBase;
import UserPages.DashBoardPg;
import UserPages.OTPInfoPage;
import UserPages.RegisterPage;
import UserPages.RegisterPageExporter;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class TC08_Register_UserAs_Exporter extends TestBase {


    @Test(priority = '1')
    public void register_new_user_Exporter() throws InterruptedException {

        DashBoardPg dashBoardPg = PageFactory.initElements(driver, DashBoardPg.class);
        RegisterPage regpg =PageFactory.initElements(driver, RegisterPage.class);
        dashBoardPg.clickOnRegisterBtn();
        Thread.sleep(5000);
        dashBoardPg.selectIndividualType();
        Thread.sleep(5000);
        RegisterPageExporter exporegpg =dashBoardPg.selectTypeAsExporter();

        Thread.sleep(4000);
        regpg.typeFullname("Sadisha Randika Helaviru Exporter User ");
        Thread.sleep(2000);
        regpg.typeNicnumber();
        Thread.sleep(2000);
        regpg.typeManualMobileNumber("0790000006");
        Thread.sleep(2000);
        regpg.typeAddressLane1("no 25 ,Abc lane ,Nugegoda,Colombo ");
        Thread.sleep(2000);



        exporegpg.clickOnProvinceExporter("Central");
        Thread.sleep(2000);
        exporegpg.selectDistrictExporter("Kandy");
        Thread.sleep(4000);
        exporegpg.selectCityExporter("Akurana");
        Thread.sleep(4000);
        exporegpg.selectDivisionalSecretarantExporter("Akurana");
        Thread.sleep(4000);
        exporegpg.selectGramaniladariExporter("Balakaduwa");
        Thread.sleep(2000);
        regpg.enterUserName();
        Thread.sleep(2000);
        regpg.schollDownTosignUpBtn();
        Thread.sleep(2000);
        exporegpg.clickVerifyLargeScaleExporter();
        Thread.sleep(2000);
        regpg.clickOnSignINBtn();
        Thread.sleep(5000);




    }
    @AfterMethod
    public  void enter_otp() throws InterruptedException
    {
        RegisterPageExporter exporegpg = PageFactory.initElements(driver, RegisterPageExporter.class);
        OTPInfoPage otppg           = PageFactory.initElements(driver, OTPInfoPage.class);
        Thread.sleep(2000);
        exporegpg.clickOnOTPinput1_Exporter();
        Thread.sleep(2000);
        exporegpg.enterOTPinput1_Exporter("0");
        Thread.sleep(2000);
        exporegpg.enterOTPinput2_Exporter("0");
        Thread.sleep(2000);
        exporegpg.enterOTPinput3_Exporter("0");
        Thread.sleep(2000);
        exporegpg.enterOTPinput4_Exporter("0");
        Thread.sleep(2000);
        exporegpg.enterOTPinput5_Exporter("0");
        Thread.sleep(2000);
        exporegpg.enterOTPinput6_Exporter("0");
        Thread.sleep(2000);
        exporegpg.clickOnOTPverifyBTN();
        Thread.sleep(5000);
       // Assert.assertEquals(otppg.verifyMessage(),"Thank you for registering with Helaviru. Your account will be reviewed and approved shortly.");


    }




}
