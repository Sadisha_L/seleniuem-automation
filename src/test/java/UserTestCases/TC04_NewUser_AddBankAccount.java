package UserTestCases;

import BaseClasses.TestBase;
import UserPages.BankAccountPage;
import UserPages.OverviewPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.awt.*;

public class TC04_NewUser_AddBankAccount extends TestBase {




    @Test(priority = 1)
    public void add_bank_account() throws InterruptedException, AWTException {
        OverviewPage overviewPage=PageFactory.initElements(driver,OverviewPage .class);
        Navigate_To_MyAccount.navigate_to_overview();
        BankAccountPage bankAccountPage =overviewPage.clickOnBankAccountMenuItem();
        Thread.sleep(7000);
        bankAccountPage.clickOnaddBankaccount();
        Thread.sleep(5000);
        bankAccountPage.selectNameOftheBank("HSBC ");
        Thread.sleep(5000);
        bankAccountPage.selectBankBranch("BAMBALAPITIYA No123Bauddhaloka MawathaColombo 04");
        Thread.sleep(8000);
        bankAccountPage.selectAccountType("Current Account");
        Thread.sleep(5000);
        bankAccountPage.enterBankAccountNumber("4589789");
        Thread.sleep(5000);
        bankAccountPage.enterAccountHolderName("Sadisha Lokuyaddehige");
        Thread.sleep(2000);
        bankAccountPage.addPassbookImages("C:\\Users\\sadisha\\IdeaProjects\\HelaviruFrameWork\\src\\test\\Utill\\images\\download.jpg");
        Thread.sleep(2000);
        bankAccountPage.clickOnAddBtn();


    }


    @AfterTest
    public  void teardown()
    {
        driver.quit();
    }





}
