package UserPages;

import BaseClasses.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.awt.*;


public class ProductPage extends TestBase {
    
    


    @FindBy(xpath = "//div[@class='flexrow ng-star-inserted']")
    private WebElement selectTypeAsSpot;

    @FindBy(xpath = "//select[@id='selectCategory']")
    private WebElement selectMainCategory;

    @FindBy(xpath = "//select[@id='selectSubCategory']")
    private WebElement selectSubcategory;


    @FindBy(xpath = "//a[text()=' Next']")
    private WebElement catnextbtn;


    @FindBy(xpath = "/html/body/app-root/app-my-account/div[2]/div/div/div[2]/app-farmer-sell-a-product/div/app-farmer-spot-selling/div/div/form/div[3]/div/input")
    private WebElement calander;

   @FindBy(xpath = "//p[text()='Auction Product Listing']")
   private WebElement selectTypeAsAution;

    @FindBy(xpath = "//input[@id='auctionPeriodTo']")
    private WebElement autionPeriedTo;

    @FindBy(xpath = "//input[@id='minimumBidValue']")
    private WebElement minimumBidValue;

    @FindBy(xpath = "//input[@id='title']")
   private WebElement titleOfProduct;

    @FindBy(xpath = "//textarea[@id='description']")
    private WebElement description;

    @FindBy(xpath = "//input[@id='buyDate']")
    private WebElement setExpiarydate;

    @FindBy(xpath = "//input[@id='qty']")
    private WebElement totalQuentity;


    @FindBy(xpath = "//select[@id='unitType']")
    private WebElement unitType;

    @FindBy(xpath = "//a[@class='btn next-btn-half']")
    private  WebElement clickOnNextBtnAdd;


    @FindBy(xpath = "//input[@class='form-control ng-untouched ng-pristine ng-valid']")
    private WebElement setDelivaryRateSelf;

    @FindBy(xpath = "/html/body/app-root/app-my-account/div[2]/div/div/div[2]/app-farmer-sell-a-product/div/app-farmer-auction/div/div[2]/form/div[5]/a[2]")
    private WebElement addresspagenxtbtn;



    @FindBy(xpath = "/html/body/app-root/app-my-account/div[2]/div/div/div[2]/app-farmer-sell-a-product/div/app-farmer-auction/div/div/div[2]/a[2]")
    private  WebElement delivaryrateNxtBtn;

    @FindBy(xpath = "/html/body/app-root/app-my-account/div[2]/div/div/div[2]/app-farmer-sell-a-product/div/app-farmer-auction/div/div/form/div[4]/a[2]")
    private  WebElement clickNextBtnListPage;

    @FindBy(xpath = "//em[@class='fa fa-plus']")
    private WebElement productimage;


    @FindBy(xpath = "/html/body/app-root/app-my-account/div[2]/div/div/div[2]/app-farmer-sell-a-product/div/app-farmer-auction/div/div/div[2]/a[2]")
    private WebElement imageuploadNxtBtn;

    @FindBy(xpath = "/html/body/app-root/app-my-account/div[2]/div/div/div[2]/app-farmer-sell-a-product/div/app-farmer-auction/div/div/div/h3")
    private WebElement successmsg;

    @FindBy(xpath = "/html/body/app-root/app-my-account/div[2]/div/div/div[2]/app-farmer-sell-a-product/div/app-farmer-auction/div/div/div/form/div/div/mat-checkbox/label/div")
    private  WebElement checkDelivaryoption;
    
    public ProductPage clickonSpotSellingBtn() throws InterruptedException {
        Thread.sleep(3000);
        selectTypeAsSpot.click();
        return this;
    }

    public ProductPage selectMainCategory(String s) throws InterruptedException {

        selectMainCategory.click();
        Thread.sleep(5000);
        BasePage.spanElement(s);
        return this;
    }

    public ProductPage selectSubCategory(String value1) throws InterruptedException {

        selectSubcategory.click();
        Thread.sleep(2000);
        BasePage.spanElement(value1);
        return this;
    }

    public ProductPage clickOn_addNewProduct_nextBtn() throws InterruptedException {
        Thread.sleep(3000);
        BasePage.element_click_with_execute_javaScirpt(catnextbtn);
        return this;
    }

    public ProductPage clickOnCalander() throws InterruptedException {


        WebElement textbox = driver.findElement(By.xpath("//input[@id='buyDate']"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].enabled = true", textbox);
        Thread.sleep(2000);

         textbox.sendKeys("2021-04-20");

        return this;
    }

    public ProductPage clickOnYearDropdown() throws InterruptedException {

        Thread.sleep(2000);
        return this;

    }


    public ProductPage selectTypeAsAution() throws InterruptedException
    {
        selectTypeAsAution.click();
        Thread.sleep(2000);
        return this;
    }

    public ProductPage addAutionPeriedToDate(String s) throws InterruptedException {
        Thread.sleep(4000);
        autionPeriedTo.sendKeys(s);
        return this;
    }

    public ProductPage enterMinimumBidValue(String s) throws InterruptedException {
        Thread.sleep(2000);
         minimumBidValue.click();
        Thread.sleep(2000);
        minimumBidValue.sendKeys(s);
        return this;
    }



    public ProductPage selectMainCategoryAuction(String s) throws InterruptedException
    {

        selectMainCategory.click();
        Thread.sleep(5000);
        BasePage.spanElementAuction(s);
        return this;
    }
    public ProductPage selectSubCategoryAuction(String s) throws InterruptedException {

        selectSubcategory.click();
        Thread.sleep(2000);
        BasePage.spanElementAuction(s);
        return this;
    }


    public ProductPage addtitleToproduct(String value1) throws InterruptedException {

        titleOfProduct.click();
        Thread.sleep(2000);
        titleOfProduct.sendKeys(value1);
        return this;
    }

    public ProductPage addDescription(String s) throws InterruptedException {

        description.click();
        Thread.sleep(2000);
        description.sendKeys(s);
        return this;
    }

    public ProductPage setExpiarydatesToProduct(String s) throws InterruptedException {
        Thread.sleep(2000);
        setExpiarydate.sendKeys(s);
        return this;
    }

    public ProductPage availableTotalQuentity(String s) throws InterruptedException {
        totalQuentity.click();
        Thread.sleep(2000);
        totalQuentity.sendKeys(s);
        return this;
    }

    public ProductPage selectUnitType(String s) throws InterruptedException {
        unitType.click();
        Thread.sleep(2000);
        BasePage.spanElementAuction(s);
        return this;
    }

    public ProductPage clickONNextBtnInAddressVerify() throws InterruptedException {

         Thread.sleep(1000);
         BasePage.element_click_with_execute_javaScirpt(clickNextBtnListPage);
         return this;

    }

    public ProductPage selectDelivaryOptnSelf(String s) throws InterruptedException {


        checkDelivaryoption.click();
        Thread.sleep(3000);
        setDelivaryRateSelf.click();
        Thread.sleep(3000);
        setDelivaryRateSelf.sendKeys(s);
        return this;

    }

    public ProductPage ClickOnAddressVerifypageNxtBtn() throws InterruptedException {
        Thread.sleep(5000);

        BasePage.element_click_with_execute_javaScirpt(addresspagenxtbtn);
        return this;
    }

    public ProductPage clickonDelivaryRateNextBtn() throws InterruptedException {
        Thread.sleep(2000);
        BasePage.element_click_with_execute_javaScirpt(delivaryrateNxtBtn);
        return this;

    }

    public ProductPage clickOnFileAddButton(String s) throws InterruptedException, AWTException {
        productimage.click();
        Thread.sleep(2000);
        BasePage.fileUpload(s);
        return this;
    }

    public ProductPage fileuploadNextBtn() throws InterruptedException {

        Thread.sleep(2000);
        BasePage.element_click_with_execute_javaScirpt(imageuploadNxtBtn);
        return this;
    }

    public String verifyProductCreatedSuccesfully() {

        String value1=successmsg.getText();
        System.out.println("Msg value is ==="+value1);
        return successmsg.getText();
    }
}
