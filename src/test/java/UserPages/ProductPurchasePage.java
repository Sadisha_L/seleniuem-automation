package UserPages;

import BaseClasses.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import static UserTestCases.ValidateSellerAbleTobuyProductsViaBank.abcd;

public class ProductPurchasePage extends TestBase

{
    @FindBy(xpath = "/html/body/app-root/app-home/div[1]/div[2]/div/div/div[2]/div/button[1]")
   private WebElement  buyBtn;

     @FindBy(xpath = "/html/body/app-root/app-products/div/div/div/div/div/div[3]/div[1]/div[1]/div/div/div[2]/div/div/a[1]")
     private WebElement selectSpotType;


     @FindBy(xpath = "//button[@id='dropdownMenuButton2']")
     private WebElement openTypesdropdown;

     @FindBy(xpath = "/html/body/app-root/app-products/div/div/div/div/div/div[3]/nav/span")
     private WebElement validProductCount;

     @FindBy(xpath = "/html/body/app-root/app-products/div/div/div/div/div/div[3]/div[2]/div/div[1]/a/div/div[2]")
     private WebElement product1;

    @FindBy(xpath = "/html/body/app-root/app-products/div/div/div/div/div/div[3]/div[2]/div/div[2]/a/div/div[2]")
    private WebElement product2;

    @FindBy(xpath = "/html/body/app-root/app-products/div/div/div/div/div/div[3]/div[2]/div/div[3]/a/div/div[2]")
    private WebElement product3;

    @FindBy(xpath = "/html/body/app-root/app-products/div/div/div/div/div/div[3]/div[2]/div/div[4]/a/div/div[2]")
    private WebElement product4;

    @FindBy(xpath = "/html/body/app-root/app-products/div/div/div/div/div/div[3]/div[2]/div/div[5]/a/div/div[2]")
    private WebElement product5;



    @FindBy(xpath = "//button[@class='darkbtn']")
    private WebElement productBuybtn;


    @FindBy(xpath = "/html/body/app-root/app-product-view-spot/div[4]/div/div/div[2]/div/div[2]/div/mat-radio-group/div[2]/div/div/mat-radio-button/label/div/div[2]")
    private  WebElement transportTpe;

    @FindBy(xpath = "//a[@class='btn next-btn-half btn-trn']")
    private WebElement proceedBtn;

    @FindBy(xpath = "//button[@class='btn-login ng-star-inserted']")
    private  WebElement paynwBtn;


    @FindBy(xpath = "//button[@value='Pay with Payment Page']")
    private WebElement bankconfirmBtn;


    @FindBy(xpath = "//input[@id='cardNumber']")
    private WebElement mastercardNumber;

    @FindBy(xpath = "//select[@id='expiryMonth']")
    private WebElement clickExparyMonth;

    @FindBy(xpath = "")
    private WebElement setExpirayMonth;

    @FindBy(xpath = "//select[@id='expiryYear']")
    private WebElement clickExparyYear;

    @FindBy(xpath = "")
    private  WebElement setExpirayYear;

    @FindBy(xpath = "//input[@id='cardHolderName']")
    private WebElement cardholderName;

    @FindBy(xpath = "//input[@id='csc']")
    private WebElement securityCode;


    @FindBy(xpath = "/html/body/div/div[9]/div[3]/button[1]")
    private WebElement pawnowBtn;



    @FindBy(xpath = "/html/body/div/div/div[3]/center/form/table/tbody/tr[13]/td/input")
    private WebElement emulatersubmitbtn;


    @FindBy(xpath = "/html/body/app-root/app-products/div/div/div/div/div/div[3]/div[2]/div/div[1]/a/div/div[2]/div[2]/div[1]/div[3]/div[2]/span")
    private WebElement ValueOfelement;


    @FindBy(xpath = "//div[@class='col-lg-4']")
    private WebElement typeAsBankTransfer;

    @FindBy(xpath = "/html/body/app-root/app-checkout/div[7]/div/div/form/div/div/input")
    private WebElement paymentReferencenumber;


    @FindBy(xpath = "/html/body/app-root/app-checkout/div[7]/div/div/form/div/div[2]/button[2]")
    private WebElement referencedoneBtn;


    @FindBy(xpath = "//a[@class='btn next-btn-half btn-trn']")
    private WebElement delivaryproceedBtn;

    @FindBy(xpath = "//button[@class='btn-login ng-star-inserted']")
    private WebElement clickPaynowBtn;

    @FindBy(xpath = "/html/body/app-root/app-checkout/div/div/div/div/div/div[2]/div/div[7]/div/div/ul/li[2]")
    private  WebElement typeAsCashDeposit;


    @FindBy(xpath = "/html/body/app-root/app-my-account/div/div/div/div/div/div/app-my-purchases/div/div[2]/div/table/tbody/tr/td[2]")
    private WebElement orderNumber;

    @FindBy(xpath = "//input[@id='radio2']")
    private WebElement typeAsBankTransferBoc;


    @FindBy(xpath = "//button[@class='btn-login ng-star-inserted']")
    private WebElement banktransferProceedBtn;


    public ProductPurchasePage clickOnBuyBtn() throws InterruptedException {
       buyBtn.click();
       Thread.sleep(4000);
        return this;
    }

    public ProductPurchasePage selectBuyingTypeAsSPOT() throws InterruptedException {
        openTypesdropdown.click();
        Thread.sleep(3000);
        selectSpotType.click();
        return this;
    }

    public String calculateNumberOfProducts() throws InterruptedException {
        Thread.sleep(3000);

        return validProductCount.getText();
    }





    public  String test ()
    {
        String[] arrOfStr = getValues();
        String firstValue = arrOfStr[0];
        String secondValue = arrOfStr[1];
        System.out.println("Value of the element is ===== "+firstValue);
        System.out.println("Value of the element is ===== "+secondValue);
        return secondValue;

    }

    public String[] getValues()
    {
        System.out.println("product count equal to === " + abcd);
        String[] arrOfStr = abcd.split("[ ( )]");
        return arrOfStr;


    }

    public ProductPurchasePage selectProductTopurchase(String count1)
    {


        System.out.println("this is sample testttttttttttttttttttttttttt   ");
        int countValue=Integer.parseInt(count1);

        System.out.println("value is equel to ===== "+countValue);

        for(int row=1;row>=countValue;row++)
        {
            String value="LKR";
            String name = driver.findElement(By.xpath("/html/body/app-root/app-products/div/div/div/div/div/div[3]/div[2]/div/div["+ row +"]/a/div/div/div[2]/div/div[3]/div/span[2]/span")).getText();
            System.out.println("Value of the selected product is  = " + name);



            if (name.equals(value))
            {

            WebElement ele=driver.findElement(By.xpath("/html/body/app-root/app-products/div/div/div/div/div/div[3]/div[2]/div/div["+ row +"]"));
                try {
                    ele.click();
                    System.out.println("element is  is exit and loop breaks" + value);
                } catch (Exception e) {
                    JavascriptExecutor executor = (JavascriptExecutor) driver;
                    executor.executeScript("arguments[0].click();", ele);
                    System.out.println("element is  equel llll  value is equel to " + value);
                }


             break;
            }
             else

            {
                System.out.println("No Valid product");
            }

        }

     return this;
    }




    public ProductPurchasePage selectProduct(String productCount)
    {
        int countValue=Integer.parseInt(productCount);


        System.out.println("Product count value is  ==== "+countValue);
       for(int i=1;i<=countValue;i++)
       {


           if(driver.findElement(By.xpath("/html/body/app-root/app-products/div/div/div/div/div/div[3]/div[2]/div/div[" + i + "]/a/div/div[2]/div[2]/div[1]/div[3]/div[2]/span")).getText().equals("SOLD OUT"))
           {
               System.out.println("Product  is sold ");
               continue;
           }

               else if (driver.findElement(By.xpath("/html/body/app-root/app-products/div/div/div/div/div/div[3]/div[2]/div/div[" + i + "]/a/div/div[2]/div[2]/div[1]/div[3]/div[1]/span[1]")).getText().equals("LKR"))
           {
              driver.findElement(By.xpath("/html/body/app-root/app-products/div/div/div/div/div/div[3]/div[2]/div/div[" + i + "]/a/div/div[2]")).click();

              break;

           }

       }
        return this;
    }












    public ProductPurchasePage clickOnProductBuyBtn() throws InterruptedException {
        BasePage.UI_schollDown();
        Thread.sleep(2000);
        productBuybtn.click();
        return this;
    }

    public ProductPurchasePage selectTransporttype() throws InterruptedException {

        BasePage.ScriptExecuter(transportTpe);
        Thread.sleep(3000);
        return this;
    }

    public ProductPurchasePage clickOnProceedBtn() throws InterruptedException {
        BasePage.UI_scroll_IntoView(proceedBtn);
        Thread.sleep(2000);
        BasePage.ScriptExecuter(proceedBtn);
        Thread.sleep(5000);

        return this;
    }

    public ProductPurchasePage clickOnPayNowBtn() throws InterruptedException {

        BasePage.UI_scroll_IntoView(paynwBtn);
        Thread.sleep(3000);
        paynwBtn.click();
        return this;

    }

    public ProductPurchasePage clickOnPaymentConfirmBtn() throws InterruptedException {
        Thread.sleep(3000);
        bankconfirmBtn.click();
        return this;
    }

    public ProductPurchasePage entermasterCardNumber(String value1) throws InterruptedException {
         mastercardNumber.click();
         Thread.sleep(1000);
         mastercardNumber.sendKeys(value1);
         Thread.sleep(1000);

        return this;
    }

    public ProductPurchasePage setExpiaryMonth(String value1) throws InterruptedException {

        clickExparyMonth.click();
        Thread.sleep(3000);
        BasePage.spanElementAuction(value1);

        return this;
    }

    public ProductPurchasePage setExpiaryYear(String value1) throws InterruptedException {

        clickExparyYear.click();
        Thread.sleep(2000);
        BasePage.spanElementAuction(value1);


        return this;
    }

    public ProductPurchasePage enterCardHolderName(String value1) {

        cardholderName.click();
        cardholderName.sendKeys(value1);
        return this;
    }

    public ProductPurchasePage enterSecurityCode(String value1) {

        securityCode.click();
        securityCode.sendKeys(value1);
        return this;
    }

    public ProductPurchasePage clickonPayNOWBtn() {
        BasePage.UI_scroll_IntoView(pawnowBtn);
        pawnowBtn.click();
        return this;
    }

    public ProductPurchasePage clickonACSEmulatersubmitBtn() throws InterruptedException {
        Thread.sleep(2000);
        emulatersubmitbtn.click();

        return this;
    }

    public ProductPurchasePage selectPaymentTypeAsBankTransfer() throws InterruptedException {
        BasePage.ScriptExecuter(typeAsBankTransfer);
        Thread.sleep(1000);
        return this;
    }


    public ProductPurchasePage scrollDownToTypeBankTrnsfer() throws InterruptedException {
        BasePage.ScriptExecuter(typeAsBankTransfer);
        Thread.sleep(1000);
        return this;
    }

    public ProductPurchasePage addReferenceNumberToPayment(String value) throws InterruptedException {
        paymentReferencenumber.click();
        Thread.sleep(1000);
        paymentReferencenumber.sendKeys(value);
        return this;
    }

    public ProductPurchasePage clickOnReferenceDoneBtn() {
        referencedoneBtn.click();
        return this;
    }

    public ProductPurchasePage clickOnDeliveryProceedBtn() throws InterruptedException {
        Thread.sleep(2000);
       BasePage.UI_scroll_IntoView(delivaryproceedBtn);
        Thread.sleep(2000);
       BasePage.ScriptExecuter(delivaryproceedBtn);

        return this;
    }

    public ProductPurchasePage clickOnPayNwBtn() throws InterruptedException {
        BasePage.UI_scroll_IntoView(clickPaynowBtn);
        Thread.sleep(2000);
        BasePage.ScriptExecuter(clickPaynowBtn);
        return this;
    }



    public ProductPurchasePage selectTypeAsCashDeposit() {
        BasePage.ScriptExecuter(typeAsCashDeposit);
        return this;
    }

    public String getOrderIdnumber()
    {
        return orderNumber.getText();
    }



    public ProductPurchasePage clickOnBankTransferProceedBtn() throws InterruptedException {

        BasePage.UI_scroll_IntoView(banktransferProceedBtn);
        Thread.sleep(1000);
        BasePage.ScriptExecuter(banktransferProceedBtn);
        return this;
    }
}
