package UserPages;

import BaseClasses.TestBase;


import com.asprise.ocr.Ocr;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import java.util.List;

import org.openqa.selenium.support.FindBy;


import javax.imageio.ImageIO;

import java.awt.*;
import java.awt.image.BufferedImage;

import java.io.*;




public class MypurchasePage extends TestBase {


    @FindBy(xpath = "/html/body/app-root/app-my-account/div/div/div/div/div/div/app-my-purchases/div/div[2]/div/mat-form-field/div/div/div/input")
    private WebElement serchInputField;


    @FindBy(xpath = "//img[@alt='QR Code']")
    private WebElement QRcodeImage;

    public MypurchasePage clickon_Search_InputField() throws InterruptedException {
        Thread.sleep(3000);
        BasePage.element_click_with_execute_javaScirpt(serchInputField);
        return this;
    }

    public MypurchasePage search_ProductBy_orederId(String s) throws InterruptedException {
        Thread.sleep(3000);
        serchInputField.sendKeys(s);
        return this;
    }



    public MypurchasePage selectOrderOnOrderTable(String s) throws InterruptedException {
        Thread.sleep(3000);
        System.out.println("charater count of the string ==" + s.length());
        BasePage.findOrderFromTable(s);

        return this;
    }


    public MypurchasePage verifyOrderStatus(String s1, String s2) throws InterruptedException {
        System.out.println("Passed value for element 1 == " + s1);
        System.out.println("Passed element 1 length is === " + s1.length());
        System.out.println("Passed value for element 2 == " + s2);
        System.out.println("Passed element 2 length is ===" + s2.length());

        BasePage.getTabledate(s1, s2);

        return this;
    }





    public MypurchasePage capture_ScreenShot()
    {
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        //Copy the file to a location and use try catch block to handle exception
        try {
            FileUtils.copyFile(screenshot, new File("C:\\Users\\sadisha\\IdeaProjects\\HelaviruFrameWork\\src\\test\\Utill\\images\\ScreenShots\\"+timeStamp + ".png"));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Screen Shot succesfully captured ");
        return this;
    }


    public boolean VerifyProductContent(String aa ) throws InterruptedException {
        verifyRequestedBidStatus( aa);
        return true;
    }

    private static boolean verifyRequestedBidStatus(String productName) throws InterruptedException {

        WebElement table = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div/div/div/div/div/div/app-my-purchases/div/div[2]/div/table"));

        List<WebElement> rawVal = table.findElements(By.tagName("tr"));
        System.out.println("Number of rows are == " + rawVal.size());

        List<WebElement> colVal = table.findElements(By.tagName("td"));
        System.out.println("Number of Columns are == " + colVal.size());
        System.out.println("passed string length is  " +productName.length());

        for (int row = 1; row < rawVal.size(); row++) {

            System.out.println("Total rows :" + rawVal.size());

            for (int col = 1; col < colVal.size(); col++) {

                String name = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div/div/div/div/div/div/app-my-purchases/div/div[2]/div/table/tbody/tr[" + row + "]/td[2]")).getText();
                System.out.println("Order ID  Value of Td 2 is = " + name);
                System.out.println("Order ID  length is equel to ==" + name.length());


                /*String bid = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div/div/div/div/div/div/app-my-purchases/div/div[2]/div/table/tbody/tr[" + row + "]/td[2]")).getText();
                System.out.println("Value of Td 2 is = " + bid);*/

               /* String replacedValue = bid.replaceAll("," , "");*/

                if (name.equals(productName)) {
                    System.out.println("Element exists " + name );
                    Thread.sleep(2000);

                    boolean available = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div/div/div/div/div/div/app-my-purchases/div/div[2]/div/table/tbody/tr[" + row + "]/td[5]//img[contains(@src, \"PAID\")]")).isDisplayed();
                    System.out.println("booliean value === " +available);

                    if (available == true)
                    {
                        return true;

                    }
                    break;

                } else {
                    System.out.println("Element doesn't exist");
                }
            }
        }
        return false;
    }

    }
