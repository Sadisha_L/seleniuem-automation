package UserPages;

import BaseClasses.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPageAgroProduce extends TestBase {

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-collector/form/mat-form-field[6]/div/div[1]/div[3]/mat-select/div/div/span")
    private WebElement selectAgroproducerProvince;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-collector/form/mat-form-field[7]/div/div[1]/div[3]/mat-select/div/div/span")
    private WebElement selectAgroproducerDistrict;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-collector/form/mat-form-field[8]/div/div/div[3]/input")
    private WebElement selectAgroproducercity;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-collector/form/mat-form-field[9]/div/div/div[3]/input")
    private WebElement selectAgroproducerDivisionalSecretarant;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-collector/form/mat-form-field[10]/div/div/div[3]/input")
    private WebElement selectAgroproducerGramaniladari;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-collector/div[2]/div/div/div[2]/ng-otp-input/div/input[1]")
    private WebElement otpinputNumber1_agroProduce;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-collector/div[2]/div/div/div[2]/ng-otp-input/div/input[2]")
    private WebElement otpinputNumber2_agroProduce;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-collector/div[2]/div/div/div[2]/ng-otp-input/div/input[3]")
    private WebElement otpinputNumber3_agroProduce;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-collector/div[2]/div/div/div[2]/ng-otp-input/div/input[4]")
    private WebElement otpinputNumber4_agroProduce;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-collector/div[2]/div/div/div[2]/ng-otp-input/div/input[5]")
    private WebElement otpinputNumber5_agroProduce;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-collector/div[2]/div/div/div[2]/ng-otp-input/div/input[6]")
    private WebElement otpinputNumber6_agroProduce;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-collector/form/div/mat-checkbox")
    private WebElement verifyagroproduceagreement;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-collector/div[2]/div/div/div[3]/button")
    private WebElement otpVerifyBTN;

    public RegisterPageAgroProduce clickOnProvinceAgroProducer(String central) throws InterruptedException {
        selectAgroproducerProvince.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(central);
        return this;
    }

    public RegisterPageAgroProduce selectDistrictAgroProducer(String kandy) throws InterruptedException {
        BasePage.UI_scroll_IntoView(selectAgroproducerDistrict);
        Thread.sleep(2000);
        selectAgroproducerDistrict.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(kandy);
        return this;
    }

    public RegisterPageAgroProduce selectCityAgroProducer(String akurana) throws InterruptedException {

        selectAgroproducercity.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(akurana);

        return this;
    }

    public RegisterPageAgroProduce selectDivisionalSecretarantAgroProducer(String akurana) throws InterruptedException {

        selectAgroproducerDivisionalSecretarant.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(akurana);
        return this;
    }

    public RegisterPageAgroProduce selectGramaniladariAgroProducer(String balakaduwa) throws InterruptedException {


        selectAgroproducerGramaniladari.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(balakaduwa);
        return this;
    }

    public RegisterPageAgroProduce clickOnOTPinput1_agroProduce() {

        otpinputNumber1_agroProduce.click();
        return this;
    }

    public RegisterPageAgroProduce enterOTPinput1_agroProduce(String s) {

        otpinputNumber1_agroProduce.sendKeys(s);
        return this;
    }

    public RegisterPageAgroProduce enterOTPinput2_agroProduce(String s) {
        otpinputNumber2_agroProduce.sendKeys(s);
        return this;
    }

    public RegisterPageAgroProduce enterOTPinput3_agroProduce(String s) {
        otpinputNumber3_agroProduce.sendKeys(s);
        return this;
    }

    public RegisterPageAgroProduce enterOTPinput4_agroProduce(String s) {
        otpinputNumber4_agroProduce.sendKeys(s);
        return this;
    }

    public RegisterPageAgroProduce enterOTPinput5_agroProduce(String s) {

        otpinputNumber5_agroProduce.sendKeys(s);
        return this;
    }

    public RegisterPageAgroProduce enterOTPinput6_agroProduce(String s) {

        otpinputNumber6_agroProduce.sendKeys(s);
        return this;
    }

    public RegisterPageAgroProduce clickVerifyAgroProducerAgreemnt() {
        BasePage.UI_scroll_IntoView(verifyagroproduceagreement);
        verifyagroproduceagreement.click();
        return this;
    }

    public RegisterPageAgroProduce clickOnOTPverifyBTN() {
        otpVerifyBTN.click();
        return this;
    }
}
