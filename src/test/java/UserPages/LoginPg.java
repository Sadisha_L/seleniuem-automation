package UserPages;

import BaseClasses.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LoginPg extends TestBase {


    @FindBy(xpath = "//a[@id='loginIcon']//span[@class='loginspan']")
    private WebElement spanLogin;

    @FindBy(name = "username")
    private WebElement txtUsername;

    @FindBy(id = "loginUserPassword")
    private WebElement txtPassword;

    @FindBy(xpath = "//button[@type='submit'][contains(.,'Sign In')]")
    private WebElement btnLogin;

    @FindBy(xpath = "/html/body/div[5]/div/div/div")
    private WebElement alertMessage;

    @FindBy(xpath = "//div[@role='alertdialog']")
    private WebElement alertMessage2;


   @FindBy(xpath ="/html/body/app-root/app-home/app-header/div[3]/div/div/div[2]/ng-otp-input/div/input[1]" )
    private WebElement otp1;

    @FindBy(xpath ="/html/body/app-root/app-home/app-header/div[3]/div/div/div[2]/ng-otp-input/div/input[2]" )
    private WebElement otp2;
    
    @FindBy(xpath ="/html/body/app-root/app-home/app-header/div[3]/div/div/div[2]/ng-otp-input/div/input[3]" )
    private WebElement otp3;

    @FindBy(xpath ="/html/body/app-root/app-home/app-header/div[3]/div/div/div[2]/ng-otp-input/div/input[4]" )
    private WebElement otp4;

    @FindBy(xpath ="/html/body/app-root/app-home/app-header/div[3]/div/div/div[2]/ng-otp-input/div/input[5]" )
    private WebElement otp5;

    @FindBy(xpath ="/html/body/app-root/app-home/app-header/div[3]/div/div/div[2]/ng-otp-input/div/input[6]" )
    private WebElement otp6;


    @FindBy(xpath ="//button[@type='button'][contains(.,'Verify')]" )
    private WebElement verifyBtn;


    @FindBy(xpath ="//i[@class='material-icons']" )
    private WebElement loggedUser;


    public LoginPg clickLoginLink()
    {
        spanLogin.click();
        return this;
    }

    public LoginPg typeUserName(String username)
    {
        txtUsername.clear();
        txtUsername.sendKeys(username);
        return this;
    }

    public LoginPg typePassword(String password)
    {
        txtPassword.sendKeys(password);
        return this;
    }

    public LoginPg clickLoginButton()
    {
        btnLogin.click();
        return this;
    }




    public String getErrorMessage()
    {
        return alertMessage.getText();
    }



    public LoginPg clickUserName()
    {
        txtUsername.click();
        txtUsername.clear();
        return this;
    }
    public  LoginPg clickOtpField1() throws InterruptedException {
         otp1.click();
        Thread.sleep(2000);
        return this;
    }

    public LoginPg typePasswordField1(String s) {
        otp1.sendKeys(s);
        return this;
    }

    public LoginPg typePasswordField2(String s)
    {
        otp2.sendKeys(s);
        return this;
    }

    public LoginPg typePasswordField3(String s)
    {
        otp3.sendKeys(s);
        return this;
    }
    public LoginPg typePasswordField4(String s) {
        otp4.sendKeys(s);
        return this;
    }

    public LoginPg typePasswordField5(String s)
    {
        otp5.sendKeys(s);
        return this;
    }
    public LoginPg typePasswordField6(String s) {
        otp6.sendKeys(s);
        return this;
    }



    public DashBoardPg clickLoginButtonSuccess()
    {
        btnLogin.click();
        return PageFactory.initElements(driver, DashBoardPg.class);
    }

    public LoginPg clickVerifyBtn()

    {
        verifyBtn.click();
        return this;

    }



    public String getErrorMessage2()
    {
        return alertMessage2.getText();
    }

    public DashBoardPg navigateToDashBoardPg() throws InterruptedException {
        Thread.sleep(5000);
        //loggedUser.click();

        Actions act =  new Actions(driver);
        act.moveToElement(loggedUser).click().perform();

        Thread.sleep(3000);
        return PageFactory.initElements(driver, DashBoardPg.class);
    }
}
