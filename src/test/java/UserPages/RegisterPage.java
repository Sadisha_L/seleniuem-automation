package UserPages;

import BaseClasses.GenarateValues;
import BaseClasses.TestBase;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class RegisterPage extends TestBase {

    @FindBy(xpath = "//*[@id=\"_name\"]")
    private WebElement fullname;


    @FindBy(xpath = "//input[@id='_mobile']")
    private WebElement mobileNumber;

    @FindBy(xpath = "//input[@id='_nic']")
    private WebElement nicNumber;


    @FindBy(xpath = "//input[@formcontrolname='address1'][contains(@id,'address1')]")
    private WebElement address1;


    @FindBy(xpath = "//input[contains(@formcontrolname,'username')]")
    private  WebElement UserName;

    @FindBy(xpath = "//span[@class='mat-button-wrapper'][contains(.,'SIGN UP')]")
    private WebElement signUpBtn;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-farmer/div[2]/div/div/div[2]/h3")
    private WebElement sign;



    @FindBy(xpath = "//input[@formcontrolname='nic'][contains(@id,'nic')]")
    private WebElement nicNumber2;


    @FindBy (xpath = "//input[@formcontrolname='mobile'][contains(@id,'mobile')]")
    private WebElement mobileNumber2;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-retailer/div[2]/div/div/div[2]/h3")
    private WebElement ele1Otp;



    @FindBy(xpath = "//input[contains(@formcontrolname,'username')]")
    private WebElement scroll_to_Username;

    @FindBy(xpath = "//input[@id='_nic']")
    private WebElement retailernicnumber;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-retailer/form/mat-form-field[3]/div/div[1]/div[3]")
    private WebElement retailerMobileNumberSelect;

    @FindBy(xpath = "//input[@id='_address1']")
    private  WebElement addresslane2select;

    @FindBy(xpath ="/html/body/app-root/app-home/app-header/div[3]/div/div/div[2]/ng-otp-input/div/input[1]" )
    private WebElement otp1;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/app-header/div[3]/div/div/div[2]/ng-otp-input/div/input[1]")
    private WebElement otpinputNumber1;

    public RegisterPage typeFullname(String name) throws InterruptedException {
        fullname.sendKeys(name);
        Thread.sleep(2000);
        return this;
    }

    public RegisterPage typeNicnumber() throws InterruptedException {

        String number=GenarateValues.genarateNICnumber();
        nicNumber.sendKeys(number);
        Thread.sleep(2000);
        return this;
    }



    public RegisterPageFarmer typeAddressLane1(String addess)
    {

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)");
        address1.sendKeys(addess);
        return PageFactory.initElements(driver, RegisterPageFarmer.class);
    }

    public RegisterPage enterUserName() throws InterruptedException
    {

        BasePage.UI_scroll_IntoView(scroll_to_Username);
        Thread.sleep(3000);
        String nicNum=GenarateValues.genarateusername();
        UserName.sendKeys(nicNum);
        return this;
    }


    public void schollDownTosignUpBtn() throws InterruptedException {
        Thread.sleep(2000);
        BasePage.UI_scroll_IntoView(signUpBtn);

    }


    public RegisterPage clickOnSignINBtn() throws InterruptedException {
        Thread.sleep(2000);
        signUpBtn.click();
        return this;
    }

    public RegisterPage typeNICnumber2() throws InterruptedException {
        retailernicnumber.click();
        Thread.sleep(2000);
        String number=GenarateValues.genarateNICnumber();
        nicNumber2.sendKeys(number);
        return this;
    }

    public RegisterPageRetailer typeAddressLane2(String addess) throws InterruptedException {

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,300)");
        Thread.sleep(2000);
        addresslane2select.click();
        Thread.sleep(2000);
        address1.sendKeys(addess);

        return PageFactory.initElements(driver, RegisterPageRetailer.class);



    }


    public RegisterPage typeManualMobileNumber(String vslue) throws InterruptedException {


        mobileNumber.sendKeys(vslue);
        Thread.sleep(2000);
        return this;
    }


    public OTPInfoPage navigateToOTPpage() throws InterruptedException
    {

        return PageFactory.initElements(driver, OTPInfoPage.class);
    }


    public void scrollToUp() {

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,-400)");
    }
}
