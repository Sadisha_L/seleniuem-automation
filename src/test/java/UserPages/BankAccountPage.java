package UserPages;

import BaseClasses.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.awt.*;

public class BankAccountPage extends TestBase {


   @FindBy(xpath = "//div[@data-toggle='modal'][contains(.,'Bank Account')]")
   private WebElement bankccount;

    @FindBy(xpath = "//select[@id='bank']")
    private WebElement bankname;

    @FindBy(xpath = "//select[@id='bankBranch']")
    private WebElement bankbranch;


    @FindBy(xpath = "//select[@id='accountType']")
    private WebElement accounttype;


    @FindBy(xpath = "//input[@id='accountNo']")
    private WebElement accountNumber;


    @FindBy(xpath = "//em[@class='fa fa-plus']")
    private WebElement passbookimage;

    @FindBy(xpath = "//html/body/app-root/app-my-account/div[1]/div/div/div/div/div/app-bank-account/div[2]/div/div/form/div[2]/div[2]/button[2]")
    private  WebElement addBtn;

     @FindBy(xpath = "//input[@id='holderName']")
     private  WebElement accountHoldername;

    public BankAccountPage clickOnaddBankaccount() throws InterruptedException {
        bankccount.click();
        Thread.sleep(2000);
        return this;

    }

    public BankAccountPage selectNameOftheBank(String value1) throws InterruptedException {

        bankname.click();
        Thread.sleep(3000);
        BasePage.selectOtionValuesInBankAccountPage(value1);

        return this;
    }

    public BankAccountPage selectBankBranch(String value1) throws InterruptedException {
        bankbranch.click();
        Thread.sleep(3000);
        BasePage.selectOtionValuesInBankAccountPage(value1);

        return this;
    }


    public BankAccountPage selectAccountType(String value1) throws InterruptedException {
        accounttype.click();
        Thread.sleep(3000);
        BasePage.selectOtionValuesInBankAccountPage(value1);
        return this;
    }

    public BankAccountPage enterBankAccountNumber(String s) throws InterruptedException {
        Thread.sleep(2000);
        accountNumber.click();
        accountNumber.sendKeys(s);
        return this;
    }

    public BankAccountPage addPassbookImages(String s) throws InterruptedException, AWTException {
        BasePage.UI_scroll_IntoView(addBtn);
        Thread.sleep(2000);
        passbookimage.click();
        Thread.sleep(2000);
        BasePage.fileUpload(s);
        return this;
    }

    public BankAccountPage enterAccountHolderName(String s) throws InterruptedException {
        accountHoldername.click();
        Thread.sleep(2000);
        accountHoldername.sendKeys(s);
        return this;
    }

    public BankAccountPage clickOnAddBtn() throws InterruptedException {
        Thread.sleep(2000);
        addBtn.click();
        Thread.sleep(8000);
        return this;
    }
}
