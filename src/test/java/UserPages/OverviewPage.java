package UserPages;

import BaseClasses.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OverviewPage extends TestBase {


  @FindBy(xpath = "/html/body/app-root/app-my-account/div[1]/div/div/div/div/div/app-farmer-overview/div[1]/div/div/div[1]/div[1]/h2")
   private WebElement ele1;



  @FindBy(xpath = "//a[contains(.,'My Bank Account')]")
  private WebElement myBankAccount;


  @FindBy(xpath = "//a[@id='sellAProduct']")
  private WebElement sellProduct;


 @FindBy(xpath = "/html/body/app-root/app-my-account/div[1]/div/div/div/div/div/div/app-sidebar/div[2]/a[2]")
  private WebElement myProductsLink;

 @FindBy(xpath = "//a[text()=' My Purchases ']")
 private  WebElement mypurchasemenuItem;

    @FindBy(xpath = "//a[text()=' View My Bids']")
 private WebElement viewMybids;


    public String VerifyNavigationToOverviewPage()
    {
        ele1.getText();
        System.out.println("value of the content is == " + ele1);
        return ele1.getText();
    }

    public BankAccountPage clickOnBankAccountMenuItem() throws InterruptedException {
        BasePage.UI_scroll_IntoView(myBankAccount);
        Thread.sleep(2000);
        myBankAccount.click();
        Thread.sleep(2000);
        return PageFactory.initElements(driver, BankAccountPage.class);
    }


    public ProductPage clickOnSellProductBtn() throws InterruptedException {
        sellProduct.click();
        Thread.sleep(5000);
        return PageFactory.initElements(driver, ProductPage.class);


    }

    public OverviewPage navigateToMyProduct() throws InterruptedException {
        Thread.sleep(5000);
        myProductsLink.click();
        Thread.sleep(2000);
        return this;
    }

    public OverviewPage selectValuesfromProductspage(String productname) throws InterruptedException
    {
        Thread.sleep(2000);
        BasePage.selectPolicies(productname );
        return this;
    }

    public MypurchasePage click_buyingMypurchases_MenuItem() throws InterruptedException {
        Thread.sleep(2000);
        mypurchasemenuItem.click();
        return PageFactory.initElements(driver, MypurchasePage.class);
    }

    public OverviewPage scrollDown_To_Element() {
        BasePage.UI_scroll_IntoView(mypurchasemenuItem);

        return this;
    }

    public MybidsPage clickOnViewmyBidsBtn() throws InterruptedException {
        viewMybids.click();
        Thread.sleep(5000);
        viewMybids.click();
        return PageFactory.initElements(driver, MybidsPage.class);
    }
}
