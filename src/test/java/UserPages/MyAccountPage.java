package UserPages;

import BaseClasses.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class MyAccountPage extends TestBase {



    @FindBy(xpath = "//a[text()=' View My Bids']")
    private WebElement mybidsMenuitem;


    @FindBy(xpath = "//input[@id='mat-input-4']")
    private WebElement searchInputfield;


    public MyAccountPage navigateToMyBidsUI() throws InterruptedException {
        BasePage.element_click_with_execute_javaScirpt(mybidsMenuitem);
        Thread.sleep(2000);
        return this;
    }

    public MyAccountPage searchForPlacedBids(String s) throws InterruptedException {
        Thread.sleep(3000);
        BasePage.element_click_with_execute_javaScirpt(searchInputfield);
        Thread.sleep(2000);
        searchInputfield.sendKeys(s);
        Thread.sleep(2000);
        return this;
    }


    public String verifyBitIsavailable(String policyvalue) throws InterruptedException {
        String productvalue=policyvalue;


        WebElement table = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div[1]/div/div/div/div/div/app-bids-auctions/div[1]/div/div/div/div/div[2]/div/div/table"));

        List<WebElement> rawVal = table.findElements(By.tagName("tr"));
        System.out.println("Number of rows are == " + rawVal.size());

        List<WebElement> colVal = table.findElements(By.tagName("td"));
        System.out.println("Number of Columns are == " + colVal.size());

        String status = "";

        L : for (int row = 1; row < rawVal.size(); row++) {

                System.out.println("Total rows :" + rawVal.size());
                String name = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div[1]/div/div/div/div/div/app-bids-auctions/div[1]/div/div/div/div/div[2]/div/div/table/tbody/tr[" + row + "]/td[1]")).getText();
                System.out.println("Value of Td 2 is = " + name);

               System.out.println("This is statment need to verify "+name.equals(productvalue));

                if (name.equals(productvalue))
                {
                    System.out.println("Element exists " + name);
                    Thread.sleep(2000);

                    status = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div[1]/div/div/div/div/div/app-bids-auctions/div[1]/div/div/div/div/div[2]/div/div/table/tbody/tr[" + row + "]/td[1]")).getText();

                    System.out.println("Element is exist & loop breaks  " + status);
                    break L;

                }
                else
                    {
                    System.out.println("Element doesn't exist");
                }
            }


        System.out.println("Status:" + status);
        return status;


    }
}
