package UserPages;

import BaseClasses.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPageServiceprovider  extends TestBase {




    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-service-provider/form/mat-form-field[6]/div/div[1]/div[3]/mat-select/div/div[1]/span")
    private WebElement selectServiceProviderProvince;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-service-provider/form/mat-form-field[7]/div/div[1]/div[3]/mat-select/div/div[1]/span")
    private WebElement selectServiceProviderDistrict;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-service-provider/form/mat-form-field[8]/div/div/div[3]/input")
    private WebElement selectServiceProvidercity;



    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-service-provider/form/mat-form-field[9]/div/div/div[3]/input")
    private WebElement selectServiceProviderDivisionalSecretarant;



    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-service-provider/form/mat-form-field[10]/div/div/div[3]/input")
    private WebElement selectServiceProviderGramaniladari;



    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-service-provider/form/div/mat-checkbox")
    private WebElement verifyServiceProvideragreement;






    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-service-provider/div[2]/div/div/div[2]/ng-otp-input/div/input[1]")
    private WebElement otpinputNumber1_ServiceProvider;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-service-provider/div[2]/div/div/div[2]/ng-otp-input/div/input[2]")
    private WebElement otpinputNumber2_ServiceProvider;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-service-provider/div[2]/div/div/div[2]/ng-otp-input/div/input[3]")
    private WebElement otpinputNumber3_ServiceProvider;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-service-provider/div[2]/div/div/div[2]/ng-otp-input/div/input[4]")
    private WebElement otpinputNumber4_ServiceProvider;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-service-provider/div[2]/div/div/div[2]/ng-otp-input/div/input[5]")
    private WebElement otpinputNumber5_ServiceProvider;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-service-provider/div[2]/div/div/div[2]/ng-otp-input/div/input[6]")
    private WebElement otpinputNumber6_ServiceProvider;

    @FindBy(xpath = "//input[@id='mat-input-2']")
    private WebElement fullname;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-service-provider/form/div[1]/div/div[1]/mat-checkbox/label/div")
    private WebElement serviceTypes;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-service-provider/div[2]/div/div/div[3]/button")
    private WebElement otpVerifyBTN;


    public RegisterPageServiceprovider clickOnProvinceServiceProvider(String central) throws InterruptedException {

        selectServiceProviderProvince.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(central);

        return this;
    }

    public RegisterPageServiceprovider selectDistrictServiceProviderr(String kandy) throws InterruptedException {
        BasePage.UI_scroll_IntoView(selectServiceProviderDistrict);
        Thread.sleep(4000);
        selectServiceProviderDistrict.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(kandy);

        return this;
    }

    public RegisterPageServiceprovider selectCityServiceProvider(String akurana) throws InterruptedException {

        selectServiceProvidercity.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(akurana);
        return this;
    }

    public RegisterPageServiceprovider selectDivisionalSecretarantServiceProvider(String akurana) throws InterruptedException {

        selectServiceProviderDivisionalSecretarant.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(akurana);

        return this;
    }

    public RegisterPageServiceprovider selectGramaniladariServiceProvider(String balakaduwa) throws InterruptedException {


        selectServiceProviderGramaniladari.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(balakaduwa);
        return this;
    }

    public RegisterPageServiceprovider clickVerifyServiceProvider() {

        BasePage.UI_scroll_IntoView(verifyServiceProvideragreement);
        verifyServiceProvideragreement.click();
        return this;
    }

    public RegisterPageServiceprovider clickOnOTPinput1_ServiceProvider() {
        otpinputNumber1_ServiceProvider.click();
        return this;
    }

    public RegisterPageServiceprovider enterOTPinput1_ServiceProvider(String s) {
        otpinputNumber1_ServiceProvider.sendKeys(s);
        return this;
    }

    public RegisterPageServiceprovider enterOTPinput2_ServiceProvider(String s) {
        otpinputNumber2_ServiceProvider.sendKeys(s);
        return this;
    }

    public RegisterPageServiceprovider enterOTPinput3_ServiceProvider(String s) {
        otpinputNumber3_ServiceProvider.sendKeys(s);
        return this;
    }

    public RegisterPageServiceprovider enterOTPinput4_ServiceProvider(String s) {
        otpinputNumber4_ServiceProvider.sendKeys(s);
        return this;
    }

    public RegisterPageServiceprovider enterOTPinput5_ServiceProvider(String s) {
        otpinputNumber5_ServiceProvider.sendKeys(s);
        return this;
    }

    public RegisterPageServiceprovider enterOTPinput6_ServiceProvider(String s) {
        otpinputNumber6_ServiceProvider.sendKeys(s);
        return this;
    }

    public RegisterPageServiceprovider enterName(String s) throws InterruptedException {

        fullname.click();
        Thread.sleep(1000);
        fullname.sendKeys(s);
        return this;
    }

    public RegisterPageServiceprovider serviceTypes() {

        serviceTypes.click();
        return this;
    }

    public RegisterPageServiceprovider clickOnOTPverifyBTN() {

        otpVerifyBTN.click();
        return this;
    }
}
