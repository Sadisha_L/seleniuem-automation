package UserPages;

import BaseClasses.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPageExporter  extends TestBase {





    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-exporter/form/mat-form-field[6]/div/div[1]/div[3]/mat-select/div/div[1]/span")
    private WebElement selectExporterProvince;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-exporter/form/mat-form-field[7]/div/div[1]/div[3]/mat-select/div/div[1]/span")
    private WebElement selectExporterDistrict;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-exporter/form/mat-form-field[8]/div/div/div[3]/input")
    private WebElement selectExportercity;



    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-exporter/form/mat-form-field[9]/div/div/div[3]/input")
    private WebElement selectExporterDivisionalSecretarant;



    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-exporter/form/mat-form-field[10]/div/div/div[3]/input")
    private WebElement selectExporterGramaniladari;



    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-exporter/form/div/mat-checkbox")
    private WebElement verifyExporteragreement;






    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-exporter/div[2]/div/div/div[2]/ng-otp-input/div/input[1]")
    private WebElement otpinputNumber1_Exporter;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-exporter/div[2]/div/div/div[2]/ng-otp-input/div/input[2]")
    private WebElement otpinputNumber2_Exporter;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-exporter/div[2]/div/div/div[2]/ng-otp-input/div/input[3]")
    private WebElement otpinputNumber3_Exporter;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-exporter/div[2]/div/div/div[2]/ng-otp-input/div/input[4]")
    private WebElement otpinputNumber4_Exporter;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-exporter/div[2]/div/div/div[2]/ng-otp-input/div/input[5]")
    private WebElement otpinputNumber5_Exporter;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-exporter/div[2]/div/div/div[2]/ng-otp-input/div/input[6]")
    private WebElement otpinputNumber6_Exporter;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-exporter/div[2]/div/div/div[3]/button")
    private WebElement otpVerifyBTN;


    public RegisterPageExporter clickOnProvinceExporter(String central) throws InterruptedException {

        selectExporterProvince.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(central);

        return this;
    }

    public RegisterPageExporter selectDistrictExporter(String kandy) throws InterruptedException {

        BasePage.UI_scroll_IntoView(selectExporterDistrict);
        Thread.sleep(4000);
        selectExporterDistrict.click();
        Thread.sleep(4000);
        BasePage.spanElementRegisterPage(kandy);
        return this;
    }

    public RegisterPageExporter selectCityExporter(String akurana) throws InterruptedException {

        selectExportercity.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(akurana);
        return this;
    }

    public RegisterPageExporter selectDivisionalSecretarantExporter(String akurana) throws InterruptedException {

        selectExporterDivisionalSecretarant.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(akurana);
        return this;
    }

    public RegisterPageExporter selectGramaniladariExporter(String balakaduwa) throws InterruptedException {

        selectExporterGramaniladari.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(balakaduwa);
        return this;
    }

    public RegisterPageExporter clickVerifyLargeScaleExporter() {
        BasePage.UI_scroll_IntoView(verifyExporteragreement);
        verifyExporteragreement.click();
        return this;
    }

    public RegisterPageExporter clickOnOTPinput1_Exporter() {
        otpinputNumber1_Exporter.click();
        return this;
    }








    public RegisterPageExporter enterOTPinput1_Exporter(String s) {
        otpinputNumber1_Exporter.sendKeys(s);
        return this;
    }

    public RegisterPageExporter enterOTPinput2_Exporter(String s) {
        otpinputNumber2_Exporter.sendKeys(s);
        return this;
    }

    public RegisterPageExporter enterOTPinput3_Exporter(String s) {
        otpinputNumber3_Exporter.sendKeys(s);
        return this;
    }

    public RegisterPageExporter enterOTPinput4_Exporter(String s) {
        otpinputNumber4_Exporter.sendKeys(s);
        return this;
    }

    public RegisterPageExporter enterOTPinput5_Exporter(String s) {
        otpinputNumber5_Exporter.sendKeys(s);
        return this;
    }

    public RegisterPageExporter enterOTPinput6_Exporter(String s) {
        otpinputNumber6_Exporter.sendKeys(s);
        return this;
    }

    public RegisterPageExporter clickOnOTPverifyBTN() {

        otpVerifyBTN.click();
        return this;
    }
}
