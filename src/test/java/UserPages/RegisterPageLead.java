package UserPages;

import BaseClasses.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPageLead extends TestBase {
    @FindBy(xpath = "//input[@id='mat-input-2']")
    private WebElement fullname;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-agent/form/mat-form-field[6]/div/div[1]/div[3]/mat-select/div/div[1]/span")
    private WebElement selectLeadProvince;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-agent/form/mat-form-field[7]/div/div[1]/div[3]/mat-select/div/div[1]/span")
    private WebElement selectLeadDistrict;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-agent/form/mat-form-field[8]/div/div/div[3]/input")
    private WebElement selectLeadrcity;



    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-agent/form/mat-form-field[9]/div/div/div[3]/input")
    private WebElement selectLeadDivisionalSecretarant;



    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-agent/form/mat-form-field[10]/div/div/div[3]/input")
    private WebElement selectLeadGramaniladari;



    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-agent/form/div[4]/mat-checkbox/label/div")
    private WebElement verifyLeadagreement;






    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-agent/div[2]/div/div/div[2]/ng-otp-input/div/input[1]")
    private WebElement otpinputNumber1_Lead;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-agent/div[2]/div/div/div[2]/ng-otp-input/div/input[2]")
    private WebElement otpinputNumber2_Lead;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-agent/div[2]/div/div/div[2]/ng-otp-input/div/input[3]")
    private WebElement otpinputNumber3_Lead;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-agent/div[2]/div/div/div[2]/ng-otp-input/div/input[4]")
    private WebElement otpinputNumber4_Lead;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-agent/div[2]/div/div/div[2]/ng-otp-input/div/input[5]")
    private WebElement otpinputNumber5_Lead;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-agent/div[2]/div/div/div[2]/ng-otp-input/div/input[6]")
    private WebElement otpinputNumber6_Lead;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-agent/form/div[1]/mat-checkbox/label/div")
    private  WebElement verifyexams;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-agent/form/div[3]/mat-checkbox/label/div")
    private WebElement verifyextraActivities;


    @FindBy(xpath = "//input[@id='mat-input-2']")
    private  WebElement leadusername;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-agent/div[2]/div/div/div[3]/button")
    private  WebElement leadotpverifybtn;

    public RegisterPageLead enterName(String s)

    {
        leadusername.click();
        leadusername.sendKeys(s);

        return this;


    }

    public RegisterPageLead clickOnProvinceLead(String central) throws InterruptedException {
        selectLeadProvince.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(central);
        return this;
    }

    public RegisterPageLead selectDistrictLead(String kandy) throws InterruptedException {

        BasePage.UI_scroll_IntoView(selectLeadDistrict);
        Thread.sleep(4000);
        selectLeadDistrict.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(kandy);
        System.out.println();
        return this;
    }

    public RegisterPageLead selectCityLead(String akurana) throws InterruptedException {
        selectLeadrcity.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(akurana);
        return this;
    }

    public RegisterPageLead selectDivisionalSecretarantLead(String akurana) throws InterruptedException {
        selectLeadDivisionalSecretarant.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(akurana);

        return this;
    }

    public RegisterPageLead selectGramaniladariLead(String balakaduwa) throws InterruptedException {

        selectLeadGramaniladari.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(balakaduwa);

        return this;
    }

    public RegisterPageLead clickVerifyLead() {
        BasePage.UI_scroll_IntoView(verifyLeadagreement);
        verifyLeadagreement.click();

        return this;
    }

    public RegisterPageLead clickOnOTPinput1_Lead() {
        otpinputNumber1_Lead.click();
        return this;
    }

    public RegisterPageLead enterOTPinput1_Lead(String s) {
        otpinputNumber1_Lead.sendKeys(s);
        return this;
    }

    public RegisterPageLead enterOTPinput2_Lead(String s) {
        otpinputNumber2_Lead.sendKeys(s);
        return this;
    }

    public RegisterPageLead enterOTPinput3_Lead(String s) {
        otpinputNumber3_Lead.sendKeys(s);
        return this;
    }

    public RegisterPageLead enterOTPinput4_Lead(String s) {
        otpinputNumber4_Lead.sendKeys(s);
        return this;
    }

    public RegisterPageLead enterOTPinput5_Lead(String s) {
        otpinputNumber5_Lead.sendKeys(s);
        return this;
    }

    public RegisterPageLead enterOTPinput6_Lead(String s) {
        otpinputNumber6_Lead.sendKeys(s);
        return this;
    }

    public RegisterPageLead verifyexams()
    {
        verifyexams.click();
        return this;
    }

    public RegisterPageLead verifyExtraActivities() {

        BasePage.element_click_with_execute_javaScirpt(verifyextraActivities);

        return this;
    }

    public RegisterPageLead clickOnVerifyBtn()
    {
        BasePage.element_click_with_execute_javaScirpt(leadotpverifybtn);
        return this;
    }
}
