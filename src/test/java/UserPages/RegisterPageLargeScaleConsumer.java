package UserPages;

import BaseClasses.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPageLargeScaleConsumer extends TestBase {

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-consumer/form/mat-form-field[6]/div/div[1]/div[3]/mat-select/div/div[1]/span")
    private WebElement selectLargeScaleConsumerProvince;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-consumer/form/mat-form-field[7]/div/div[1]/div[3]/mat-select/div/div[1]/span")
    private WebElement selectLargeScaleConsumerDistrict;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-consumer/form/mat-form-field[8]/div/div/div[3]/input")
    private WebElement selectLargeScaleConsumercity;



    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-consumer/form/mat-form-field[9]/div/div/div[3]/input")
    private WebElement selectLargeScaleConsumerDivisionalSecretarant;



    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-consumer/form/mat-form-field[10]/div/div/div[3]/input")
    private WebElement selectLargeScaleConsumerGramaniladari;



    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-consumer/form/div/mat-checkbox")
    private WebElement verifyLargeScaleConsumeragreement;






    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-consumer/div[2]/div/div/div[2]/ng-otp-input/div/input[1]")
    private WebElement otpinputNumber1_LargeScaleConsumer;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-consumer/div[2]/div/div/div[2]/ng-otp-input/div/input[2]")
    private WebElement otpinputNumber2_LargeScaleConsumer;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-consumer/div[2]/div/div/div[2]/ng-otp-input/div/input[3]")
    private WebElement otpinputNumber3_LargeScaleConsumer;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-consumer/div[2]/div/div/div[2]/ng-otp-input/div/input[4]")
    private WebElement otpinputNumber4_LargeScaleConsumer;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-consumer/div[2]/div/div/div[2]/ng-otp-input/div/input[5]")
    private WebElement otpinputNumber5_LargeScaleConsumer;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-consumer/div[2]/div/div/div[2]/ng-otp-input/div/input[6]")
    private WebElement otpinputNumber6_LargeScaleConsumer;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-consumer/div[2]/div/div/div[3]/button")
    private WebElement otpVerifyBTN;


    public RegisterPageLargeScaleConsumer clickOnProvinceLargeScaleConsumer(String central) throws InterruptedException {

        selectLargeScaleConsumerProvince.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(central);


        return this;
    }

    public RegisterPageLargeScaleConsumer selectDistrictLargeScaleConsumer(String kandy) throws InterruptedException {

        BasePage.UI_scroll_IntoView(selectLargeScaleConsumerDistrict);
        Thread.sleep(4000);
        selectLargeScaleConsumerDistrict.click();
        Thread.sleep(3000);
        BasePage.spanElementRegisterPage(kandy);

        return this;
    }

    public RegisterPageLargeScaleConsumer selectCityLargeScaleConsumer(String akurana) throws InterruptedException {
        selectLargeScaleConsumercity.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(akurana);




        return this;
    }

    public RegisterPageLargeScaleConsumer selectDivisionalSecretarantLargeScaleConsumer(String akurana) throws InterruptedException {

        selectLargeScaleConsumerDivisionalSecretarant.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(akurana);

        return this;
    }

    public RegisterPageLargeScaleConsumer selectGramaniladariLargeScaleConsumer(String balakaduwa) throws InterruptedException {

        selectLargeScaleConsumerGramaniladari.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(balakaduwa);
        return this;
    }

    public RegisterPageLargeScaleConsumer clickVerifyLargeScaleConsumerAgreemnt()

    {
        BasePage.UI_scroll_IntoView(verifyLargeScaleConsumeragreement);
        verifyLargeScaleConsumeragreement.click();
        return this;
    }

    public RegisterPageLargeScaleConsumer clickOnOTPinput1_LargeScaleConsumer() {
        otpinputNumber1_LargeScaleConsumer.click();
        return this;
    }

    public RegisterPageLargeScaleConsumer enterOTPinput1_LargeScaleConsumer(String s) {

        otpinputNumber1_LargeScaleConsumer.sendKeys(s);
        return this;
    }

    public RegisterPageLargeScaleConsumer enterOTPinput2_LargeScaleConsumer(String s) {
        otpinputNumber2_LargeScaleConsumer.sendKeys(s);
        return this;
    }

    public RegisterPageLargeScaleConsumer enterOTPinput3_LargeScaleConsumer(String s) {
        otpinputNumber3_LargeScaleConsumer.sendKeys(s);
        return this;
    }

    public RegisterPageLargeScaleConsumer enterOTPinput4_LargeScaleConsumer(String s) {
        otpinputNumber4_LargeScaleConsumer.sendKeys(s);
        return this;
    }

    public RegisterPageLargeScaleConsumer enterOTPinput5_LargeScaleConsumer(String s) {
        otpinputNumber5_LargeScaleConsumer.sendKeys(s);
        return this;
    }

    public RegisterPageLargeScaleConsumer enterOTPinput6_LargeScaleConsumer(String s) {
        otpinputNumber6_LargeScaleConsumer.sendKeys(s);
        return this;
    }


    public RegisterPageLargeScaleConsumer clickOnOTPverifyBTN() {

        otpVerifyBTN.click();
        return this;
    }
}
