package UserPages;

import BaseClasses.GenarateValues;
import BaseClasses.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPageRetailer extends TestBase {

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-retailer/form/mat-form-field[6]/div/div[1]/div[3]")
    private WebElement selectretailerProvince;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-retailer/form/mat-form-field[7]/div/div[1]/div[3]")
    private WebElement DistrictRetail;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-retailer/form/mat-form-field[8]/div/div[1]/div[3]")
    private WebElement cityRetail;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-retailer/form/mat-form-field[9]/div/div[1]/div[3]")
    private WebElement divRetail;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-retailer/form/mat-form-field[10]/div/div[1]/div[3]")
    private WebElement grmaRetail;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-retailer/form/p/mat-checkbox/label/div")
    private  WebElement export;

    @FindBy(xpath = "//input[contains(@formcontrolname,'username')]")
    private WebElement usernameRetailer;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-retailer/form/div[1]/mat-selection-list/mat-list-option[1]/div/mat-pseudo-checkbox")
    private  WebElement natureofFruits;

    @FindBy(xpath = "//span[@class='mat-button-wrapper'][contains(.,'SIGN UP')]")
    private WebElement signUpBtn;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-retailer/form/div[2]/mat-checkbox/label/div")
    private WebElement retailerverifyAgreemnt;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-retailer/div[2]/div/div/div[2]/ng-otp-input/div/input[1]")
    private WebElement otpinputNumber1_Retailer;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-retailer/div[2]/div/div/div[2]/ng-otp-input/div/input[2]")
    private WebElement otpinputNumber2_Retailer;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-retailer/div[2]/div/div/div[2]/ng-otp-input/div/input[3]")
    private WebElement otpinputNumber3_Retailer;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-retailer/div[2]/div/div/div[2]/ng-otp-input/div/input[4]")
    private WebElement otpinputNumber4_Retailer;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-retailer/div[2]/div/div/div[2]/ng-otp-input/div/input[5]")
    private WebElement otpinputNumber5_Retailer;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-retailer/div[2]/div/div/div[2]/ng-otp-input/div/input[6]")
    private WebElement otpinputNumber6_Retailer;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-retailer/div[2]/div/div/div[3]/button")
    private WebElement otpVerifyBTN;



    public RegisterPageRetailer clickOnProvinceRetailer(String central) throws InterruptedException {

        selectretailerProvince.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(central);
        return this;

    }

    public RegisterPageRetailer selectDistrictRetailer(String kandy) throws InterruptedException {

        BasePage.UI_scroll_IntoView(DistrictRetail);
        Thread.sleep(1000);
        DistrictRetail.click();
        Thread.sleep(5000);
        BasePage.spanElementRegisterPage(kandy);
        return this;
    }

    public RegisterPageRetailer selectCityRetailer(String akurana) throws InterruptedException {
        Thread.sleep(2000);
        cityRetail.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(akurana);
        return this;



    }

    public RegisterPageRetailer selectDivisionalSecretarantRetailer(String akurana) throws InterruptedException {

        divRetail.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(akurana);
        return this;


    }

    public RegisterPageRetailer selectGramaniladariRetailer(String balakaduwa) throws InterruptedException {

        grmaRetail.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(balakaduwa);
        return this;

    }

    public RegisterPageRetailer enterUserNameRetailer() throws InterruptedException {

        BasePage.UI_scroll_IntoView(usernameRetailer);
        Thread.sleep(2000);
        String username= GenarateValues.genarateusername();
        Thread.sleep(2000);
        usernameRetailer.click();
        Thread.sleep(2000);
        usernameRetailer.sendKeys(username);
        return this;
    }

    public RegisterPageRetailer checkNatureOfFruitType() throws InterruptedException {

        BasePage.UI_scroll_IntoView(natureofFruits);
        Thread.sleep(2000);
        natureofFruits.click();
        return this;

    }

    public RegisterPageRetailer checkExport() throws InterruptedException {

        Thread.sleep(2000);
        export.click();
        return this;


    }

    public RegisterPageRetailer schollDownTosignUpBtn() throws InterruptedException {
        Thread.sleep(2000);
        BasePage.UI_scroll_IntoView(signUpBtn);
        return this;
    }

    public RegisterPageRetailer clickverifyAgreementRetiler() throws InterruptedException {

        Thread.sleep(4000);
        BasePage.UI_scroll_IntoView(retailerverifyAgreemnt);
        Thread.sleep(2000);
        retailerverifyAgreemnt.click();
        return this;
    }

    public RegisterPageRetailer clickOnSignINBtn() throws InterruptedException {
        Thread.sleep(2000);
        signUpBtn.click();
        return this;

    }

    public RegisterPageRetailer clickOnOTPinput1_Retailer() {

        otpinputNumber1_Retailer.click();

        return this;
    }

    public RegisterPageRetailer enterOTPinput1_Retailer(String s) {
        otpinputNumber1_Retailer.sendKeys(s);
        return this;
    }

    public RegisterPageRetailer enterOTPinput2_Retailer(String s) {
        otpinputNumber2_Retailer.sendKeys(s);
        return this;
    }

    public RegisterPageRetailer enterOTPinput3_Retailer(String s) {
        otpinputNumber3_Retailer.sendKeys(s);
        return this;
    }

    public RegisterPageRetailer enterOTPinput4_Retailer(String s) {
        otpinputNumber4_Retailer.sendKeys(s);
        return this;
    }

    public RegisterPageRetailer enterOTPinput5_Retailer(String s) {
        otpinputNumber5_Retailer.sendKeys(s);
        return this;
    }

    public RegisterPageRetailer enterOTPinput6_Retailer(String s) {
        otpinputNumber6_Retailer.sendKeys(s);
        return this;
    }

    public RegisterPageRetailer clickOnOTPverifyBTN() {
         otpVerifyBTN.click();
        return this;
    }
}
