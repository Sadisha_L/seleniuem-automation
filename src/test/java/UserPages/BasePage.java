package UserPages;

import BaseClasses.TestBase;
import org.openqa.selenium.*;



import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class BasePage extends TestBase
{

    public BasePage(WebDriver driver1) {
        this.driver=driver1;
    }

    public static void UI_scroll_IntoView(final WebElement element)
    {

        try {

            JavascriptExecutor executor = (JavascriptExecutor)driver;
            executor.executeScript("arguments[0].scrollIntoView();", element);
        } catch (Exception e) {
            System.err.format("" + e);
        }



    }







    public  static  void selectOtionValuesInBankAccountPage(String value1) throws InterruptedException {
        try {
            String abc=value1;
            WebElement  ele1=driver.findElement(By.xpath("//option[text()='"+abc+"']"));
            Thread.sleep(2000);
            ele1.click();
        } catch (NoSuchElementException e)
        {
            System.err.format("" + e);
        }
    }


    public static void fileUpload (String path) throws AWTException {
        StringSelection strSelection = new StringSelection(path);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(strSelection, null);

        Robot robot = new Robot();

        robot.delay(300);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.delay(200);
        robot.keyRelease(KeyEvent.VK_ENTER);
    }





    public static void spanElement(String value1)
    {

        try {
            String abc=value1;
            WebElement  ele1=driver.findElement(By.xpath("//option[@class='ng-star-inserted'][span[text()='"+abc+"']]"));
            ele1.click();
        } catch (NoSuchElementException e)
        {
            System.err.format("" + e);
        }

    }



    public static void spanElementAuction(String value1)
    {

        try {
            String abc=value1;
            WebElement  ele1=driver.findElement(By.xpath("//option[text()='"+abc+"']"));
            ele1.click();
        } catch (NoSuchElementException e)
        {
            System.err.format("" + e);
        }

    }



    public static void element_click_with_execute_javaScirpt(final WebElement element)
    {
        try
        {
            element.click();
        } catch (Exception e) {
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", element);
            System.out.println("naviagate to this  =============   ");
        }

    }

    public static void selectPolicies(String policyvalue) throws InterruptedException {
        Thread.sleep(3000);
        String value = policyvalue;

        WebElement table = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div[1]/div/div/div/div/div/app-farmer-my-products/div/div/div/div/div/div[2]/div/div/mat-horizontal-stepper/div[2]/div[1]/form/table"));
        List<WebElement> allrows = table.findElements(By.tagName("tr"));
        System.out.println("Number of rows are == " + allrows.size());
        List<WebElement> allrows2 = table.findElements(By.tagName("td"));
        System.out.println("Number of Columns are == " + allrows2.size());


       for (int row = 1; row < allrows.size(); row++)
        {
            System.out.println("Total rows :" + allrows.size());

            String name = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div[1]/div/div/div/div/div/app-farmer-my-products/div/div/div/div/div/div[2]/div/div/mat-horizontal-stepper/div[2]/div[1]/form/table/tbody/tr[" + row + "]/td[1]/div")).getText();
            System.out.println("Value of Td 2 is = " + name);

            if (name.equals(value))
            {
                Thread.sleep(2000);

                System.out.println("element is  is exit and loop breaks");
                Thread.sleep(2000);
                break ;
            } else {
                System.out.println("Element doesn't exist");
            }
        }

    }



    public  static void spanElementRegisterPage(String value1)
    {

        try {
            String abc=value1;
            WebElement  ele1=driver.findElement(By.xpath("//span[(text()= '"+abc+"')]"));
            ele1.click();
        } catch (NoSuchElementException e)
        {
            System.err.format("element didn't found" + e);
        }

    }


public static  void UI_schollDown()
{
    JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("window.scrollBy(0,300)");

}


public  static void ScriptExecuter(WebElement element)
{
    WebElement ele3=element;
    try {
        ele3.click();
    } catch (Exception e) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", ele3);

    }
}


    public static String findOrderFromTable(String abc) throws InterruptedException {
        String productvalue=abc;


        WebElement table = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div/div/div/div/div/div/app-my-purchases/div/div[2]/div/table"));

        List<WebElement> rawVal = table.findElements(By.tagName("tr"));
        System.out.println("Number of rows are == " + rawVal.size());

        List<WebElement> colVal = table.findElements(By.tagName("td"));
        System.out.println("Number of Columns are == " + colVal.size());

        String status = "";

        L : for (int row = 1; row < rawVal.size(); row++) {

            System.out.println("Total rows :" + rawVal.size());
            String name = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div/div/div/div/div/div/app-my-purchases/div/div[2]/div/table/tbody/tr[" + row + "]/td[2]")).getText();
            System.out.println("Value of Td 2 is = " + name);
            System.out.println("Seleted string value from  table "+name.length());

            System.out.println("This is statment need to verify "+name.equals(productvalue));

            if (name.equals(productvalue))
            {
                System.out.println("Element exists  and validate " + name);
                Thread.sleep(2000);

                status = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div/div/div/div/div/div/app-my-purchases/div/div[2]/div/table/tbody/tr[" + row + "]/td[2]")).getText();
               // driver.findElement(By.xpath("/html/body/app-root/app-my-account/div/div/div/div/div/div/app-my-purchases/div/div[2]/div/table/tbody/tr[" + row + "]/td[2]")).click();

                System.out.println("Element is exist & loop breaks  " + status);
                break L;

            }
            else
            {
                System.out.println("Element doesn't exist");
            }
        }


        System.out.println("Status:" + status);
        return status;


    }

    public static void scrollUpToTop()
    {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,-350)");

    }

    public static String[] getTabledate(String s1,String s2) throws InterruptedException {
        String[] ans=new String[2];



        WebElement table = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div/div/div/div/div/div/app-my-purchases/div/div[2]/div/table"));

        List<WebElement> rawVal = table.findElements(By.tagName("tr"));
        System.out.println("Number of rows are == " + rawVal.size());

        List<WebElement> colVal = table.findElements(By.tagName("td"));
        System.out.println("Number of Columns are == " + colVal.size());



        L : for (int row = 1; row < rawVal.size(); row++) {

            System.out.println("Total rows :" + rawVal.size());
            String paymentStatus = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div/div/div/div/div/div/app-my-purchases/div/div[2]/div/table/tbody/tr[" + row + "]/td[5]")).getText();
            System.out.println("Value of Td 5 is = " + paymentStatus);
            System.out.println("Selected string value from  table "+paymentStatus.length());
            System.out.println("This is statement need to verify "+paymentStatus.equals(s1));



            String orderStatus = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div/div/div/div/div/div/app-my-purchases/div/div[2]/div/table/tbody/tr[" + row + "]/td[6]")).getText();
            System.out.println("Value of Td 6 is = " + orderStatus);
            System.out.println("Selected string value from  table "+orderStatus.length());
            System.out.println("This is statement need to verify "+orderStatus.equals(s2));

            if (paymentStatus.equals(s1) && orderStatus.equals(s2))
            {
                System.out.println("Element exists  and validate " + paymentStatus);
                Thread.sleep(2000);

                ans[0] = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div/div/div/div/div/div/app-my-purchases/div/div[2]/div/table/tbody/tr[" + row + "]/td[2]")).getText();

                System.out.println("paymentStatus Element is found  & loop breaks  " + ans[0]);

                ans[1] = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div/div/div/div/div/div/app-my-purchases/div/div[2]/div/table/tbody/tr[" + row + "]/td[2]")).getText();

                System.out.println("OrderStatus element is found  & loop breaks  " + ans[0]);
                break L;

            }
            else
            {
                System.out.println("Element doesn't exist");
            }
        }


        System.out.println("Status:" + ans);
        return ans;






    }

    public static String findOrderFromBIdTable(String s) throws InterruptedException {
        String ans=s;
        System.out.println("Passed element lenght == "+ans.length());
        System.out.println("Passed element Value == "+ans);

        WebElement table = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div/div/div/div/div/div/app-bids-auctions/div/div/div/div/div/div[2]/div/div/table"));

        List<WebElement> rawVal = table.findElements(By.tagName("tr"));
        System.out.println("Number of rows are == " + rawVal.size());

        List<WebElement> colVal = table.findElements(By.tagName("td"));
        System.out.println("Number of Columns are == " + colVal.size());



        L : for (int row = 1; row < rawVal.size(); row++) {

            System.out.println("Total rows :" + rawVal.size());
            String paymentStatus = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div/div/div/div/div/div/app-bids-auctions/div/div/div/div/div/div[2]/div/div/table/tbody/tr[" + row + "]/td[1]")).getText();
            System.out.println("Value of Td 5 is = " + paymentStatus);
            System.out.println("Selected string value from  table as product === "+paymentStatus.length());
            System.out.println("product String length "+paymentStatus.equals(s));

            String paymentStatus2 = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div/div/div/div/div/div/app-bids-auctions/div/div/div/div/div/div[2]/div/div/table/tbody/tr[" + row + "]/td[6]")).getText();
            System.out.println("Value of Td 5 is = " + paymentStatus2);
            System.out.println("Selected string value from  table "+paymentStatus2.length());
            System.out.println("This is statement need to verify "+paymentStatus2.equals(s));


            if (paymentStatus.equals(s) && paymentStatus2.equals("Checkout") )
            {
                System.out.println("Element exists  and validate " + paymentStatus);
                Thread.sleep(2000);

                ans = driver.findElement(By.xpath("/html/body/app-root/app-my-account/div/div/div/div/div/div/app-bids-auctions/div/div/div/div/div/div[2]/div/div/table/tbody/tr[" + row + "]/td[6]")).getText();

                System.out.println("paymentStatus Element is found  & loop breaks  " + ans);


                break L;

            }
            else
            {
                System.out.println("Element doesn't exist");
            }
        }


        System.out.println("Status:" + ans);
        return ans;

    }
}

