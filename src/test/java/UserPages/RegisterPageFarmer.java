package UserPages;

import BaseClasses.GenarateValues;
import BaseClasses.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPageFarmer extends TestBase {


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-farmer/form/mat-form-field[6]/div/div[1]/div[3]/mat-select/div/div[1]/span")
    private WebElement selectFarmerProvince;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-farmer/form/mat-form-field[7]/div/div[1]/div[3]/mat-select/div/div[1]/span")
    private WebElement selectFarmerDistrict;


    @FindBy(xpath = "//input[contains(@formcontrolname,'city')]")
    private WebElement selectFarmerCity;


    @FindBy(xpath = "//input[contains(@formcontrolname,'division')]")
    private WebElement selectFarmerDivisionalSecretarant;


    @FindBy(xpath = "//input[contains(@formcontrolname,'gramaList')]")
    private WebElement selectFarmerGramaniladari;


    @FindBy(xpath = "//input[contains(@formcontrolname,'farmerUsername')]")
    private WebElement usernameFarmer;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-farmer/form/div[1]/div[1]/div/mat-checkbox/label/div")
    private  WebElement TypeAsMahaweliFarmer;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-farmer/form/div[3]/div[1]/div/mat-checkbox/label/div")
    private  WebElement WaterResorceTypeAsNatural;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-farmer/form/div[5]/div[1]/div/mat-checkbox/label/div")
    private WebElement  selectCultivatedCropsType;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-farmer/form/div[6]/mat-checkbox")
    private WebElement registerVerifyagreementFarmer;

    @FindBy(xpath = "//span[@class='mat-button-wrapper']")
    private WebElement signUpBtn;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-farmer/form/div[5]/div[1]/div/mat-label")
    private WebElement CultivatedCropsType;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-farmer/div[2]/div/div/div[2]/ng-otp-input/div/input[1]")
    private WebElement otpinputNumber1_Retailer;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-farmer/div[2]/div/div/div[2]/ng-otp-input/div/input[2]")
    private WebElement otpinputNumber2_Retailer;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-farmer/div[2]/div/div/div[2]/ng-otp-input/div/input[3]")
    private WebElement otpinputNumber3_Retailer;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-farmer/div[2]/div/div/div[2]/ng-otp-input/div/input[4]")
    private WebElement otpinputNumber4_Retailer;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-farmer/div[2]/div/div/div[2]/ng-otp-input/div/input[5]")
    private WebElement otpinputNumber5_Retailer;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-farmer/div[2]/div/div/div[2]/ng-otp-input/div/input[6]")
    private WebElement otpinputNumber6_Retailer;

    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-farmer/div[2]/div/div/div[3]/button")
    private WebElement otpverifyBTN;



    public RegisterPageFarmer clickOnProvinceFarmer() throws InterruptedException
    {

        selectFarmerProvince.click();
        Thread.sleep(3000);
        return this;


    }
    public RegisterPageFarmer selectProvinceFarmer(String s) throws InterruptedException {
        Thread.sleep(2000);

        BasePage.spanElementRegisterPage(s);
        Thread.sleep(2000);
        return this;
    }


    public RegisterPageFarmer selectDistrictFarmer(String s) throws InterruptedException {


        Thread.sleep(3000);
        selectFarmerDistrict.click();
        Thread.sleep(4000);
        BasePage.spanElementRegisterPage(s);
        return this;

    }

    public RegisterPageFarmer selectCityFarmer(String s) throws InterruptedException {

        BasePage.UI_scroll_IntoView(selectFarmerCity);
        Thread.sleep(2000);
        selectFarmerCity.click();
        Thread.sleep(3000);
        BasePage.spanElementRegisterPage(s);
        return this;
    }

    public RegisterPageFarmer selectDivisionalSecretarantFarmer(String s) throws InterruptedException {
        BasePage.UI_scroll_IntoView(selectFarmerDivisionalSecretarant);
        Thread.sleep(3000);
        selectFarmerDivisionalSecretarant.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(s);
        return this;
    }

    public RegisterPageFarmer selectGramaniladariFarmer(String s) throws InterruptedException {


        Thread.sleep(2000);
        selectFarmerGramaniladari.click();
        Thread.sleep(3000);
        BasePage.spanElementRegisterPage(s);
        return this;
    }


    public RegisterPageFarmer enterUsernameFarmer() throws InterruptedException {

        BasePage.UI_scroll_IntoView(usernameFarmer);
        Thread.sleep(3000);
        String nicNum= GenarateValues.genarateusername();
        usernameFarmer.sendKeys(nicNum);
        return this;
    }

    public RegisterPageFarmer selectTypeAsMahaweliFarmer() throws InterruptedException {

        Thread.sleep(2000);
        TypeAsMahaweliFarmer.click();
        return this;

    }

    public RegisterPageFarmer selectWaterResorceTypeAsNatural() throws InterruptedException {
        Thread.sleep(2000);
        WaterResorceTypeAsNatural.click();
        return this;
    }



    public RegisterPageFarmer selectCultivatedCrops() throws InterruptedException {

        BasePage.UI_scroll_IntoView(CultivatedCropsType);
        Thread.sleep(3000);
        selectCultivatedCropsType.click();
        return this;
    }

    public RegisterPageFarmer chollDownTosignUpBtn() throws InterruptedException {
        Thread.sleep(2000);
        BasePage.UI_scroll_IntoView(signUpBtn);
        return this;
    }

    public RegisterPageFarmer clickverifyAgreement() throws InterruptedException {

        BasePage.UI_scroll_IntoView(registerVerifyagreementFarmer);
        Thread.sleep(2000);
        registerVerifyagreementFarmer.click();
        return this;
    }

    public RegisterPageFarmer clickOnSignINBtn() throws InterruptedException {

        Thread.sleep(2000);
        signUpBtn.click();
        return this;
    }

    public RegisterPageFarmer clickOnOTPinput1_Retailer() throws InterruptedException {

        otpinputNumber1_Retailer.click();
        Thread.sleep(1000);
        return this;
    }

    public RegisterPageFarmer enterOTPinput1_Retailer(String s) throws InterruptedException {
        otpinputNumber1_Retailer.sendKeys(s);
        Thread.sleep(1000);
        return this;
    }

    public RegisterPageFarmer enterOTPinput2_Retailer(String s) throws InterruptedException {
        otpinputNumber2_Retailer.sendKeys(s);
        Thread.sleep(1000);
        return this;
    }

    public RegisterPageFarmer enterOTPinput3_Retailer(String s) throws InterruptedException {
        otpinputNumber3_Retailer.sendKeys(s);
        Thread.sleep(1000);
        return this;
    }

    public RegisterPageFarmer enterOTPinput4_Retailer(String s) throws InterruptedException {
        otpinputNumber4_Retailer.sendKeys(s);
        Thread.sleep(1000);
        return this;
    }

    public RegisterPageFarmer enterOTPinput5_Retailer(String s) throws InterruptedException {
        otpinputNumber5_Retailer.sendKeys(s);
        Thread.sleep(1000);
        return this;
    }

    public RegisterPageFarmer enterOTPinput6_Retailer(String s) throws InterruptedException {
        otpinputNumber6_Retailer.sendKeys(s);
        Thread.sleep(1000);
        return this;
    }

    public RegisterPageFarmer clickOnOTPverifyBTN()


    {
        otpverifyBTN.click();
        return this;
    }
}
