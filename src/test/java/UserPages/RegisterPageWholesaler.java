package UserPages;

import BaseClasses.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPageWholesaler extends TestBase {


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-whole-sale-buyer/form/mat-form-field[6]/div/div[1]/div[3]/mat-select/div/div[1]/span")
    private WebElement selectWholesalarProvince;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-whole-sale-buyer/form/mat-form-field[7]/div/div[1]/div[3]/mat-select/div/div[1]/span")
    private WebElement selectWholesalarDistrict;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-whole-sale-buyer/form/mat-form-field[8]/div/div/div[3]/input")
    private WebElement selectWholesalarcity;



    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-whole-sale-buyer/form/mat-form-field[9]/div/div/div[3]/input")
    private WebElement selectWholesalarDivisionalSecretarant;



    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-whole-sale-buyer/form/mat-form-field[10]/div/div/div[3]/input")
    private WebElement selectWholesalarGramaniladari;



    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-whole-sale-buyer/form/div/mat-checkbox")
    private WebElement verifyWholesalaragreement;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-whole-sale-buyer/div[2]/div/div/div[2]/ng-otp-input/div/input[1]")
    private WebElement otpinputNumber1_Wholesaler;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-whole-sale-buyer/div[2]/div/div/div[2]/ng-otp-input/div/input[2]")
    private WebElement otpinputNumber2_Wholesaler;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-whole-sale-buyer/div[2]/div/div/div[2]/ng-otp-input/div/input[3]")
    private WebElement otpinputNumber3_Wholesaler;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-whole-sale-buyer/div[2]/div/div/div[2]/ng-otp-input/div/input[4]")
    private WebElement otpinputNumber4_Wholesaler;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-whole-sale-buyer/div[2]/div/div/div[2]/ng-otp-input/div/input[5]")
    private WebElement otpinputNumber5_Wholesaler;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-whole-sale-buyer/div[2]/div/div/div[2]/ng-otp-input/div/input[6]")
    private WebElement otpinputNumber6_Wholesaler;


    @FindBy(xpath = "/html/body/app-root/app-sign-up/div/div/div/div/div/div[2]/div/div/app-sign-up-whole-sale-buyer/div[2]/div/div/div[3]/button")
    private WebElement otpVerifyBTN;

    public RegisterPageWholesaler clickOnProvinceWholesaler(String central) throws InterruptedException {

        selectWholesalarProvince.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(central);


        return this;
    }

    public RegisterPageWholesaler selectDistrictWholesaler(String kandy) throws InterruptedException {

        BasePage.UI_scroll_IntoView(selectWholesalarDistrict);
        Thread.sleep(3000);
        selectWholesalarDistrict.click();
        Thread.sleep(3000);
        BasePage.spanElementRegisterPage(kandy);

        return this;
    }

    public RegisterPageWholesaler selectCityAgroWholesaler(String akurana) throws InterruptedException {
        selectWholesalarcity.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(akurana);




        return this;
    }

    public RegisterPageWholesaler selectDivisionalSecretarantWholesaler(String akurana) throws InterruptedException {

        selectWholesalarDivisionalSecretarant.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(akurana);

        return this;
    }

    public RegisterPageWholesaler selectGramaniladariWholesaler(String balakaduwa) throws InterruptedException {

        selectWholesalarGramaniladari.click();
        Thread.sleep(2000);
        BasePage.spanElementRegisterPage(balakaduwa);
        return this;
    }

    public RegisterPageWholesaler clickVerifyWholesalerAgreemnt()

    {
        BasePage.UI_scroll_IntoView(verifyWholesalaragreement);
        verifyWholesalaragreement.click();
        return this;
    }

    public RegisterPageWholesaler clickOnOTPinput1_Wholesaler() {
        otpinputNumber1_Wholesaler.click();
        return this;
    }

    public RegisterPageWholesaler enterOTPinput1_Wholesaler(String s) {

        otpinputNumber1_Wholesaler.sendKeys(s);
        return this;
    }

    public RegisterPageWholesaler enterOTPinput2_Wholesaler(String s) {
        otpinputNumber2_Wholesaler.sendKeys(s);
        return this;
    }

    public RegisterPageWholesaler enterOTPinput3_Wholesaler(String s) {
        otpinputNumber3_Wholesaler.sendKeys(s);
        return this;
    }

    public RegisterPageWholesaler enterOTPinput4_Wholesaler(String s) {
        otpinputNumber4_Wholesaler.sendKeys(s);
        return this;
    }

    public RegisterPageWholesaler enterOTPinput5_Wholesaler(String s) {
        otpinputNumber5_Wholesaler.sendKeys(s);
        return this;
    }

    public RegisterPageWholesaler enterOTPinput6_Wholesaler(String s) {
        otpinputNumber6_Wholesaler.sendKeys(s);
        return this;
    }

    public RegisterPageWholesaler clickOnOTPverifyBTN() {

        otpVerifyBTN.click();
        return this;
    }
}
