package UserPages;

import BaseClasses.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MybidsPage extends TestBase {



    @FindBy(xpath = "/html/body/app-root/app-my-account/div/div/div/div/div/div/app-bids-auctions/div/div/div/div/div/div[2]/div/div/mat-form-field/div/div/div/input")
    private WebElement serchBidInputField;

    public MybidsPage searchforBId() throws InterruptedException {
        BasePage.element_click_with_execute_javaScirpt(serchBidInputField);

        Thread.sleep(3000);
        return this;
    }

    public String verifybidfromTable(String s) throws InterruptedException {

        String value  =BasePage.findOrderFromBIdTable(s);
        return value;
    }

    public MybidsPage searchforBiddedProduct(String s) {
        serchBidInputField.sendKeys(s);
        return this;
    }
}
