package UserPages;

import BaseClasses.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class DashBoardPg extends TestBase {


    @FindBy(xpath="/html/body/app-root/app-home/app-header/div[1]/div/div/div/div/div/div[2]/div/div[2]/div[2]/div/a[1]")
    private WebElement myaccountBtn;



    @FindBy(xpath = "//span[@class='flexlist borderbtn'][contains(.,'/ Register')]")
    private WebElement Registerbtn;


    @FindBy(className = "dropdown-item")
    private WebElement typeAsIndividual;


    @FindBy(xpath= "//span[text()=' Helaviru Farmer']")
    private WebElement typeAsHelaviruFarmer;


    @FindBy(xpath="//span[text()=' Retailer ']")
    private WebElement typeAsHelaviruRetailer;

    @FindBy(xpath = "//img[@alt='Logo']")
    private WebElement helaviruLogo;

    @FindBy(xpath="//span[text()=' Agro Produce Aggregator ']")
    private WebElement typeAsAgroProducer;

    @FindBy(xpath = "//span[text()=' Wholesaler ']")
    private WebElement typeAsWholesaler;

    @FindBy(xpath = "//span[text()=' Large Scale Consumer ']")
    private  WebElement typeAsLargeScaleConsumer;

    @FindBy(xpath = "//span[text()=' Exporter ']")
    private  WebElement typeAsExporter;

    @FindBy(xpath = "//span[text()=' Service Provider ']")
    private  WebElement typeAsServiceProvider;

    @FindBy(xpath = "//span[text()=' Helaviru Lead ']")
    private WebElement typeAsHelaviruLead;

    public String VerifyLandingPg() throws InterruptedException {
        Thread.sleep(2000);
        return myaccountBtn.getText();
    }

    public OverviewPage clickOnMyAccountBtn()
    {
        myaccountBtn.click();
        return PageFactory.initElements(driver, OverviewPage.class);

    }

    public DashBoardPg clickOnRegisterBtn()
    {
        Registerbtn.click();
        return this;
    }

    public DashBoardPg selectIndividualType()
    {
        typeAsIndividual.click();
        return this;
    }

    public RegisterPage selectTypeAsHelaviruFarmer()
    {
        typeAsHelaviruFarmer.click();
        return PageFactory.initElements(driver, RegisterPage.class);
    }


    public RegisterPage selectTypeAsRetailer()
    {
        typeAsHelaviruRetailer.click();
        return PageFactory.initElements(driver, RegisterPage.class);
    }

    public ProductPurchasePage navigateToLandingUI()
    {
        helaviruLogo.click();
        return PageFactory.initElements(driver, ProductPurchasePage.class);
    }

    public RegisterPageAgroProduce selectTypeAsAgroProduce()

    {
        typeAsAgroProducer.click();
        return PageFactory.initElements(driver, RegisterPageAgroProduce.class);
    }

    public RegisterPageWholesaler selectTypeAsWholesaler()

    {
        typeAsWholesaler.click();
        return PageFactory.initElements(driver, RegisterPageWholesaler.class);
    }

    public RegisterPageLargeScaleConsumer selectTypeAsLargeScaleConsumer() throws InterruptedException {

        BasePage.UI_scroll_IntoView(typeAsLargeScaleConsumer);
        Thread.sleep(2000);
        typeAsLargeScaleConsumer.click();
        return PageFactory.initElements(driver, RegisterPageLargeScaleConsumer.class);
    }

    public RegisterPageExporter selectTypeAsExporter() throws InterruptedException {

        BasePage.UI_scroll_IntoView(typeAsExporter);
        Thread.sleep(2000);
        typeAsExporter.click();
        return PageFactory.initElements(driver, RegisterPageExporter.class);
    }

    public RegisterPageServiceprovider selectTypeAsServiceProvider() throws InterruptedException {

        BasePage.UI_scroll_IntoView(typeAsServiceProvider);
        Thread.sleep(2000);
        typeAsServiceProvider.click();
        return PageFactory.initElements(driver, RegisterPageServiceprovider.class);
    }

    public RegisterPageLead selectTypeAsHelaviruLead() throws InterruptedException {
        BasePage.UI_scroll_IntoView(typeAsHelaviruLead);
        Thread.sleep(2000);
        typeAsHelaviruLead.click();
        return PageFactory.initElements(driver, RegisterPageLead.class);
    }

    public MyAccountPage clickOnMyAccountBtnSellerUser()

    {
        myaccountBtn.click();
        return PageFactory.initElements(driver, MyAccountPage.class);
    }
}
