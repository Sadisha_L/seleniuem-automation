package AdminPages;

import BaseClasses.TestBaseAdmin;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class AdminProductPage  extends TestBaseAdmin {


    @FindBy(xpath = "//div[@class='mat-form-field-infix ng-tns-c86-1']")
    private WebElement serchProduct;


    @FindBy(xpath = "//input[@id='mat-input-2']")
    private WebElement serchProduct1;


    @FindBy(xpath = "//h2[@class='font-weight-bold graytext m-0 proname']")
    private WebElement product;


    public AdminProductPage VerifyNewlyaddedPageIsInThere(String onion) throws InterruptedException {
        Thread.sleep(3000);
        serchProduct.click();
        Thread.sleep(3000);
        serchProduct1.sendKeys(onion);
        return this;
    }

    public String selectProductFromTable(String onion) throws InterruptedException {
        Thread.sleep(5000);
        String value = onion;
        System.out.println("product name value passed  to function == " +value);

        WebElement table = driver.findElement(By.xpath("/html/body/app-root/app-admin-panel/div/div/div/div/div/div/app-product-activation/div/div/div/div/div/div[2]/div/div/table"));

        List<WebElement> allrows = table.findElements(By.tagName("tr"));
        System.out.println("Number of rows are == " + allrows.size());
        List<WebElement> allrows2 = table.findElements(By.tagName("td"));
        System.out.println("Number of Columns are == " + allrows2.size());
        String value2="";


        for (int row = 1; row < allrows.size(); row++) {
            System.out.println("Total rows :" + allrows.size());
            Thread.sleep(5000);

            String name = driver.findElement(By.xpath("/html/body/app-root/app-admin-panel/div/div/div/div/div/div/app-product-activation/div/div/div/div/div/div[2]/div/div/table/tbody/tr[" + row + "]/td[1]")).getText();
            System.out.println("Value of Td 2 is = " + name);

            if (name.equals(value)) {
                Thread.sleep(2000);
                System.out.println("Row number is === " + row);
                WebElement ele = driver.findElement(By.xpath("/html/body/app-root/app-admin-panel/div/div/div/div/div/div/app-product-activation/div/div/div/div/div/div[2]/div/div/table/tbody/tr[" + row + "]/td[1]"));

                try {
                    ele.click();
                    System.out.println("element is  is exit and loop breaks" + value);
                } catch (Exception e) {
                    JavascriptExecutor executor = (JavascriptExecutor) driver;
                    executor.executeScript("arguments[0].click();", ele);
                    System.out.println("element is  equel and  value is equel to " + value);
                }

                Thread.sleep(2000);
                break;
            } else {
                System.out.println("Row number is === " + row);
                System.out.println("Element doesn't exist for selection ");
            }
        }


        return value2;
    }

    public String verifyProductName() throws InterruptedException {

        Thread.sleep(2000);
        return product.getText();
    }
}
