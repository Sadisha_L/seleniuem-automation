package AdminPages;


import BaseClasses.TestBaseAdmin;
import UserPages.BasePage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminLoginPage extends TestBaseAdmin {

    @FindBy(xpath = "//input[@name='username']")
    private WebElement username;

    @FindBy(xpath = "//input[@name='password']")
    private WebElement password;

    @FindBy(xpath = "//button[@class='btn btn-outline-primary']")
    private WebElement loginBtn;

    public AdminLoginPage enterUsername(String value1)
    {
        username.click();
        username.sendKeys(value1);
        return  this;
    }

    public AdminLoginPage enterPassword(String value1)
    {
        password.click();
        password.sendKeys(value1);
        return  this;
    }

    public AdminDashBoardPage clickOnLoginBtn() throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)");
        Thread.sleep(3000);
        BasePage.element_click_with_execute_javaScirpt(loginBtn);
        return PageFactory.initElements(driver, AdminDashBoardPage.class);
    }


}
