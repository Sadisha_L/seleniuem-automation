package AdminPages;

import BaseClasses.TestBaseAdmin;
import UserPages.BasePage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class AdminOnbordPage  extends TestBaseAdmin {

    @FindBy(xpath = "/html/body/app-root/app-admin-panel/div/div/div/div/div/div/app-onboarding/div/div/div/div/div/mat-horizontal-stepper/div[2]/div/form/div[2]/div/div/div/mat-form-field/div/div/div/input")
    private WebElement searchInputField;


    @FindBy(xpath = "//a[@class='roundbtn btn-b b-approve']")
    private WebElement viewButton;


    @FindBy(xpath = "/html/body/app-root/app-admin-panel/div/div/div/div/div/div/app-onboarding/div[2]/div/div/div[2]/div/form/div[2]/button[2]")
    private WebElement approveButton;


    @FindBy(xpath = "//h3[text()='APPROVED']")
    private WebElement message;



    public AdminOnbordPage searchOnbordingUsers(String s) throws InterruptedException {
        Thread.sleep(3000);
        BasePage.ScriptExecuter(searchInputField);

         Thread.sleep(3000);
         searchInputField.sendKeys(s);
         return this;
    }


    public AdminOnbordPage clickOnViewButton() throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)");
        Thread.sleep(3000);
        BasePage.element_click_with_execute_javaScirpt(viewButton);

        return this;
    }

    public AdminOnbordPage clickOnVerifyButton() {
        BasePage.element_click_with_execute_javaScirpt(approveButton);
        return this;
    }

    public String VerifyUserActivatedSuccesfullyMsg() {

        return  message.getText();
    }
}
