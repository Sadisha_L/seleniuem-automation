package AdminPages;


import BaseClasses.TestBaseAdmin;


import UserPages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import java.util.List;



public class AdminDashBoardPage extends TestBaseAdmin {

    @FindBy(xpath = "//h2[text()=' Overview ']")
    private WebElement VerifyadminOverviewPage;

    @FindBy(xpath = "//li[@id='dropdown2']")
    private WebElement openProduct;

    @FindBy(xpath = "//a[text()=' Product Activation ']")
    private WebElement clickOnproduct;


    @FindBy(xpath = "/html/body/app-root/app-admin-panel/div/div/div/div/div/div/app-product-activation/div/div/div/div/div/div[2]/div/div/table")
    public   WebElement tablevalue;


    @FindBy(xpath = "//h2[@class='font-weight-bold graytext m-0 proname']")
    private WebElement product;



   @FindBy(xpath = "//div[@class='mat-form-field-infix ng-tns-c86-1']")
    private  WebElement serchProduct;

   @FindBy(xpath = "//input[@id='mat-input-2']")
   private WebElement serchProduct1;


   @FindBy(xpath = "//a[@class='btn-b b-approve'][contains(.,'Activate')]")
   private WebElement approveProductBrn;


   @FindBy(xpath = "//h3[text()='SUCCESS']")
   private WebElement productactivatedSuccesmsg;

  @FindBy(xpath = "/html/body/app-root/app-admin-panel/div/div/div/div/div/div/div/app-admin-sidebar/div/div[2]/li[2]/div/div/ul/li[1]")
   private WebElement productView;

  @FindBy(xpath = "/html/body/app-root/app-admin-panel/div/div/div/div/div/div/app-admin-products/div/div/div/div/div/div[2]/div/div/mat-horizontal-stepper/div[2]/div/form/table")
  public WebElement abc;



  @FindBy(xpath = "/html/body/app-root/app-admin-panel/div/div/div/div/div/div/div/app-admin-sidebar/div/div[2]/a[3]/img")
  private  WebElement paymentVerificationmenuitem;


  @FindBy(xpath = "/html/body/app-root/app-admin-panel/div/div/div/div/div/div/app-bank-transfer/div[1]/div/div/div/div/div[2]/div/div/table/tbody/tr/td[1]")
  private WebElement transactionID;


  @FindBy(xpath = "//input[@id='mat-input-0']")
  private WebElement searchTransaction;


  @FindBy(xpath = "/html/body/app-root/app-admin-panel/div/div/div/div/div/div/div/app-admin-sidebar/div/div[2]/a[4]/img")
  private WebElement payamentHistory;


  @FindBy(xpath = "//a[@class='roundbtn btn-b b-approve']")
  private  WebElement paymentrecordviewbtn;


    @FindBy(xpath = "//input[@id='mat-input-2']")
    private  WebElement paymentRemark;

    @FindBy(xpath = "//a[text()='APPROVE']")
    private WebElement paymentApprovebtn;

    @FindBy(xpath = "//p[text()='Pending payment approved.']")
    private WebElement pendingpaymentmsg;

    @FindBy(xpath = "")
    private WebElement paymentverifictionhistorymenuItem;

    @FindBy(xpath = "//a[text()=' Onboarding Approval ']")
    private WebElement onbordingapprovalmenuItem;

    public String VerifyAdminUserLoginToSystem()
    {

        return VerifyadminOverviewPage.getText();
    }

    public AdminDashBoardPage clickPrductsmenuItem() throws InterruptedException {
        Thread.sleep(2000);
        openProduct.click();
        Thread.sleep(2000);
        return this;
    }

    public AdminProductPage clickOnProductActivationsubmenu() throws InterruptedException {
        Thread.sleep(2000);
        clickOnproduct.click();
        Thread.sleep(2000);
        return PageFactory.initElements(driver, AdminProductPage.class);
    }


    public void scrollDownInWindow()
    {

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,250)");
    }


    public AdminDashBoardPage VerifyNewlyaddedPageIsInThere(String sesame) throws InterruptedException {
        Thread.sleep(5000);
        serchProduct.click();
        Thread.sleep(3000);
        serchProduct1.sendKeys(sesame);
        return this;
    }



    public AdminDashBoardPage clickOnApproveBtn() throws InterruptedException {
        Thread.sleep(2000);
        BasePage.UI_scroll_IntoView(approveProductBrn);


        try {
            approveProductBrn.click();

        } catch (Exception e) {
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", approveProductBrn);

        }

        return this;
    }

    public String VerifySuccesmsg() throws InterruptedException {
       Thread.sleep(3000);
       return productactivatedSuccesmsg.getText();
    }

    public AdminDashBoardPage clickOnProductView() throws InterruptedException {
        Thread.sleep(2000);
        productView.click();
        return this;
    }

    public String selectProductFromTableAsActive(String onion) throws InterruptedException {

        Thread.sleep(3000);
        String value2 =onion;
        System.out.println("product name value passed  to function == " +value2);

        WebElement table = driver.findElement(By.xpath("/html/body/app-root/app-admin-panel/div/div/div/div/div/div/app-admin-products/div[1]/div/div/div/div/div[2]/div/div/mat-horizontal-stepper/div[2]/div[1]/form/table"));
         List<WebElement> allrows = table.findElements(By.tagName("tr"));
         System.out.println("Number of rows are == " + allrows.size());
        List<WebElement> allrows2 = table.findElements(By.tagName("td"));
        System.out.println("Number of Columns are == " + allrows2.size());



        for (int row = 1; row < allrows.size(); row++) {
            System.out.println("Total rows count is == :" + allrows.size());
            Thread.sleep(5000);

            String Product_name = driver.findElement(By.xpath("/html/body/app-root/app-admin-panel/div/div/div/div/div/div/app-admin-products/div[1]/div/div/div/div/div[2]/div/div/mat-horizontal-stepper/div[2]/div[1]/form/table/tbody/tr[" + row + "]/td[2]")).getText();
            System.out.println("Value of Td 2 is  to ==== " + Product_name);
            String productStatus=driver.findElement(By.xpath("/html/body/app-root/app-admin-panel/div/div/div/div/div/div/app-admin-products/div[1]/div/div/div/div/div[2]/div/div/mat-horizontal-stepper/div[2]/div[1]/form/table/tbody/tr[" + row + "]/td[10]")).getText();
            System.out.println("Value of Td 10 is equal  = " + productStatus);
            if (Product_name.equals(value2)&& productStatus.equals("Active")) {
                Thread.sleep(2000);
                System.out.println("Row number is equel  === " + row);
                WebElement ele = driver.findElement(By.xpath("/html/body/app-root/app-admin-panel/div/div/div/div/div/div/app-admin-products/div[1]/div/div/div/div/div[2]/div/div/mat-horizontal-stepper/div[2]/div[1]/form/table/tbody/tr[" + row + "]/td[10]"));

                try {
                    ele.click();
                    value2=ele.getText();
                    System.out.println("element is  is exit and loop breaks and continue == " + ele.getText());
                } catch (Exception e) {
                    System.out.println("element is  equel and  value is equel to " + ele.getText());
                    JavascriptExecutor executor = (JavascriptExecutor) driver;
                    executor.executeScript("arguments[0].click();", ele);
                    value2 =ele.getText();
                }

                Thread.sleep(2000);
                break;
            } else {
                System.out.println("Row number is === " + row);
                System.out.println("Element doesn't exist for selection ");
            }
        }


        return value2;



    }


    public AdminDashBoardPage clickOnpayment_Verification() throws InterruptedException {
        Thread.sleep(2000);
        BasePage.ScriptExecuter(paymentVerificationmenuitem);
        Thread.sleep(5000);
        return this;
    }


    public AdminDashBoardPage search_transaction(String transaction_id) throws InterruptedException {

        searchTransaction.click();
        Thread.sleep(5000);
        searchTransaction.sendKeys(transaction_id);

        return this;
    }

    public String verify_transactionIsThere() {

        return transactionID.getText();
    }

    public AdminDashBoardPage clickOnViewBtn()
    {
        paymentrecordviewbtn.click();
        return this;
    }

    public AdminDashBoardPage clickOnPaymentApproveBtn() {
        paymentApprovebtn.click();
        return this;
    }

    public AdminDashBoardPage addPaymentRemark(String value)
    {
       paymentRemark.sendKeys(value);
        return this;
    }

    public String VerifyApprovals()
    {

        return pendingpaymentmsg.getText();
    }

    public AdminDashBoardPage navigateToPaymentVerificationHistory() {
        paymentverifictionhistorymenuItem.click();
        return this;
    }

    public AdminOnbordPage navigateToOnbordingApproval()
    {
        onbordingapprovalmenuItem.click();
        return PageFactory.initElements(driver, AdminOnbordPage.class);
    }
}
